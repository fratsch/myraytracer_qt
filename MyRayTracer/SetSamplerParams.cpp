﻿#include "SetSamplerParams.h"


SetSamplerParams::SetSamplerParams( SamplerConfig* sampler_config, QWidget* parent )
	: QDialog( parent ),
	  sampler_config( sampler_config ) {

	ui.setupUi( this );

	ui.comboBox_samplerType->addItems( { "Regular", "Random", "NRook", "Jittered", "Multi-Jittered", "Hammersley" } );

	ui.comboBox_numSamples->addItems( { "1", "4", "9", "16", "25", "36", "49", "64", "81", "100" } );
	ui.comboBox_numSamples->setCurrentIndex( static_cast<int>( sqrt( static_cast<float>( sampler_config->num_samples ) ) ) - 1 );
	ui.spinBox_numSets->setValue( sampler_config->num_sets );
}


SetSamplerParams::~SetSamplerParams( ) = default;


void SetSamplerParams::on_pushButton_Cancel_Sampler_clicked( ) {
	close( );
}


void SetSamplerParams::on_pushButton_Confirm_Sampler_clicked( ) {
	sampler_config->sampler = static_cast<Samplers>( ui.comboBox_samplerType->currentIndex( ) );

	sampler_config->num_samples = ui.comboBox_numSamples->currentText( ).toInt( );
	sampler_config->num_sets = ui.spinBox_numSets->value( );
	close( );
}
