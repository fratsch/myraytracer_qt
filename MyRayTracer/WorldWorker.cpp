#include "WorldWorker.h"

#include <QDebug>

#include "World.h"
#include "Ambient.h"
#include "QTDataWrapper.h"
#include "SamplerConfig.h"
#include "CameraConfig.h"
#include "AmbientConfig.h"
#include "WorldConfig.h"
#include "ObjectEditorItem.h"
#include "LightEditorItem.h"


WorldWorker::WorldWorker( WorldConfig* world_config, SamplerConfig* sampler_config, AmbientConfig* ambient_config, CameraConfig* camera_config,
                          QObject* parent )
	: QThread( parent ),
	  sampler_config( sampler_config ),
	  camera_config( camera_config ),
	  ambient_config( ambient_config ),
	  world_config( world_config ) {}


WorldWorker::~WorldWorker( ) = default;


void WorldWorker::run( ) {

	qInfo( ) << "Render Process started!";

	World world;

	qInfo( ) << "Building World!";

	std::vector<GeometricObject*> objects;

	for( ObjectEditorItem* object : world_config->get_objects( ) ) {
		objects.push_back( object->get_object( ) );
	}

	std::vector<Light*> lights;

	for( LightEditorItem* light : world_config->get_lights( ) ) {
		lights.push_back( light->get_light( ) );
	}

	world.build( objects, lights );

	world.set_sampler( sampler_config->sampler, sampler_config->num_samples, sampler_config->num_sets );

	world.set_camera( camera_config->hres, camera_config->vres, camera_config->pixel_size, camera_config->camera,
	                  Point3D( camera_config->cam_x, camera_config->cam_y, camera_config->cam_z ),
	                  Point3D( camera_config->lookAt_x, camera_config->lookAt_y, camera_config->lookAt_z ),
	                  Vector3D( camera_config->up_x, camera_config->up_y, camera_config->up_z ), camera_config->rolling_angle,
	                  camera_config->view_distance, camera_config->zoom, camera_config->exposure, camera_config->focal_distance, camera_config->lens_radius );

	world.set_ambient( new Ambient( ambient_config->radiance_scaling_factor,
	                                RGBColor( ambient_config->amb_r, ambient_config->amb_g, ambient_config->amb_b ) ) );

	qInfo( ) << "World built!";

	qInfo( ) << "Rendering...";

	std::chrono::high_resolution_clock::time_point start = std::chrono::high_resolution_clock::now( );

	world.render_scene( );

	std::chrono::high_resolution_clock::time_point end = std::chrono::high_resolution_clock::now( );

	auto duration = std::chrono::duration_cast<std::chrono::milliseconds>( end - start ).count( );

	qInfo( ) << "Rendering done! -  Time taken: " << duration << "ms";

	QImage renderedImage = world.pixel_output.renderedImage;

	emit resultReady( renderedImage );

}
