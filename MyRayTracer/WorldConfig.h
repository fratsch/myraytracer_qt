#pragma once

#include <qlist.h>

class ObjectEditorItem;
class MaterialEditorItem;
class LightEditorItem;


class WorldConfig {

public:

	WorldConfig( );

	~WorldConfig( );

	void add_object(ObjectEditorItem* object );

	void delete_object(ObjectEditorItem* object );

	void add_material(MaterialEditorItem* material );

	void delete_material(MaterialEditorItem* material );

	void add_light(LightEditorItem* light );

	void delete_light(LightEditorItem* light );

	void delete_objects( );

	void delete_materials( );

	void delete_lights( );

	QList<ObjectEditorItem*> get_objects();

	QList<MaterialEditorItem*> get_materials();

	QList<LightEditorItem*> get_lights();

private:

	QList<ObjectEditorItem*> objects;

	QList<MaterialEditorItem*> materials;

	QList<LightEditorItem*> lights;

};


inline QList<ObjectEditorItem*> WorldConfig::get_objects( ) {
	return objects;
}

inline QList<MaterialEditorItem*> WorldConfig::get_materials() {
	return materials;
}

inline QList<LightEditorItem*> WorldConfig::get_lights( ) {
	return lights;
}

