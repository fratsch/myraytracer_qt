#pragma once

#include "ui_SetSamplerParams.h"
#include "RayTracerGUI.h"


class SamplerConfig;


class SetSamplerParams : public QDialog {

Q_OBJECT

public:

	SetSamplerParams( SamplerConfig* sampler_config, QWidget* parent = Q_NULLPTR );
	~SetSamplerParams( );

public slots:

	// cancel button action
	void on_pushButton_Cancel_Sampler_clicked( );

	// confirm button action
	void on_pushButton_Confirm_Sampler_clicked( );

private:

	Ui::SetSamplerParams ui;

	SamplerConfig* sampler_config;
};
