#pragma once


#include <QListWidgetItem>


class Light;


class LightEditorItem : public QListWidgetItem {

public:

	LightEditorItem( Light* light, QString light_name );
	~LightEditorItem( );

	Light* get_light( );

private:

	Light* light;

};


inline Light* LightEditorItem::get_light( ) {
	return light;
}
