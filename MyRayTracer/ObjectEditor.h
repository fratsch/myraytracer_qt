#pragma once


#include "ui_ObjectEditor.h"


class ObjectEditorItem;
class MaterialEditorItem;
class WorldConfig;


class ObjectEditor : public QDialog {

Q_OBJECT

public:

	ObjectEditor( ObjectEditorItem* object, QList<MaterialEditorItem*> materials, bool edit_mode, QWidget* parent = Q_NULLPTR );
	~ObjectEditor( );

public slots:

	// close button action
	void on_pushButton_Close_ObjectEditor_clicked( );

	// add/edit button action
	void on_pushButton_AddEdit_Object_clicked( );

private:

	void read_object_parameters( ObjectEditorItem* object );

	Ui::ObjectEditor ui;

	bool edit_mode;

	QList<MaterialEditorItem*> materials;

};
