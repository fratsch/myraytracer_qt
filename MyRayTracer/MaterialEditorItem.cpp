#include "MaterialEditorItem.h"


MaterialEditorItem::MaterialEditorItem( Material* material, QString material_name )
	: material( material ) {
	setText( material_name );
}

MaterialEditorItem::~MaterialEditorItem( ) {}
