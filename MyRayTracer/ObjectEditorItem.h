#pragma once


#include <QListWidgetItem>


class GeometricObject;


class ObjectEditorItem : public QListWidgetItem {

public:

	ObjectEditorItem( GeometricObject* object, QString object_name );
	~ObjectEditorItem( );

	GeometricObject* get_object( );

private:

	GeometricObject* object;

};


inline GeometricObject* ObjectEditorItem::get_object( ) {
	return object;
}
