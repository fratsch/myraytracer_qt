#pragma once

#include <QOpenGLWidget>
#include <QPainter>

class OGLWidget : public QOpenGLWidget {

Q_OBJECT

public:

	OGLWidget( QWidget* parent = Q_NULLPTR);
	~OGLWidget( );

	void clear();

protected:

	void initializeGL();
	void resizeGL(int w, int h);
	void paintGL() override;

private:

};
