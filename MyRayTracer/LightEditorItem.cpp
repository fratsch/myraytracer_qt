#include "LightEditorItem.h"


LightEditorItem::LightEditorItem( Light* light, QString light_name )
	: light( light ) {
	setText( light_name );
}

LightEditorItem::~LightEditorItem( ) {}
