#pragma once

#include "ui_SetAmbientParams.h"
#include "RayTracerGUI.h"

class AmbientConfig;


class SetAmbientParams : public QDialog {

Q_OBJECT

public:

	SetAmbientParams( AmbientConfig* ambient_config, QWidget* parent = Q_NULLPTR );
	~SetAmbientParams( );

public slots:

	// cancel button action
	void on_pushButton_Cancel_Ambient_clicked( );

	// confirm button action
	void on_pushButton_Confirm_Ambient_clicked();

private:

	Ui::SetAmbientParams ui;

	AmbientConfig* ambient_config;
};
