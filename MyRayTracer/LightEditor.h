#pragma once


#include "ui_LightEditor.h"


class WorldConfig;


class LightEditor : public QDialog {

Q_OBJECT

public:

	LightEditor( QWidget* parent = Q_NULLPTR );
	~LightEditor( );

public slots:

	// close button action
	void on_pushButton_Close_LightEditor_clicked( );

	// add/edit button action
	void on_pushButton_AddEdit_Light_clicked( );

private:

	Ui::LightEditor ui;

};
