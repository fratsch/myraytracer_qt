#pragma once


enum class Cameras {
	ORTHO ,
	PIN ,
	THIN
};


class CameraConfig {

public:

	CameraConfig( );

	~CameraConfig( );

	void set_resolution( int hr, int vr );

	void set_camera_position( float x, float y, float z );

	void set_lookAt_position( float x, float y, float z );

	void set_up_vector( float x, float y, float z );

	// camera type
	Cameras camera;

	// viewport size
	int hres, vres;

	// pixel size
	float pixel_size;

	// camera position
	float cam_x, cam_y, cam_z;

	// camera lookAt position
	float lookAt_x, lookAt_y, lookAt_z;

	// up vector
	float up_x, up_y, up_z;

	// camera rolling angle
	float rolling_angle;

	// camera view distance
	float view_distance;

	// camera zoom
	float zoom;

	// camera exposure
	float exposure;

	// camera focal distance
	float focal_distance;

	//camera lens radius
	float lens_radius;

};


inline void CameraConfig::set_resolution( const int hr, const int vr ) {
	hres = hr;
	vres = vr;
}


inline void CameraConfig::set_camera_position( const float x, const float y, const float z ) {
	cam_x = x;
	cam_y = y;
	cam_z = z;
}


inline void CameraConfig::set_lookAt_position( const float x, const float y, const float z ) {
	lookAt_x = x;
	lookAt_y = y;
	lookAt_z = z;
}


inline void CameraConfig::set_up_vector( const float x, const float y, const float z ) {
	up_x = x;
	up_y = y;
	up_z = z;
}
