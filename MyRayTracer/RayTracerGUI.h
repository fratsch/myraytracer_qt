#pragma once

#include "ui_RayTracerGUI.h"
#include <QMainWindow>
#include "SamplerConfig.h"
#include "CameraConfig.h"
#include "WorldConfig.h"

class AmbientConfig;


class RayTracerGUI : public QMainWindow {

Q_OBJECT

public:

	RayTracerGUI( QWidget* parent = Q_NULLPTR );
	~RayTracerGUI( );

public slots:

	// form slots

	void on_actionExit_triggered( );

	void on_actionStart_triggered( );

	void on_actionClear_triggered( );

	// sampler config

	void on_actionConfigure_Sampling_triggered( );

	// camera config

	void on_actionConfigure_Camera_triggered( );

	// ambient config

	void on_actionConfigure_Ambient_triggered( );

	// configure objects

	void on_actionConfigure_Objects_triggered( );

	// configure materials

	void on_actionConfigure_Materials_triggered( );

	// configure lights

	void on_actionConfigure_Lights_triggered( );

	// rendering

	void drawRenderedImage( QImage image );

private:

	Ui::RayTracerGUIClass ui;

	// pointer for sampler config

	SamplerConfig* sampler_config;

	// pointer for camera config

	CameraConfig* camera_config;

	// pointer for world config

	WorldConfig* world_config;

	//pointer for ambient config

	AmbientConfig* ambient_config;

};
