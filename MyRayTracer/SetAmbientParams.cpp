#include "SetAmbientParams.h"

#include "AmbientConfig.h"


SetAmbientParams::SetAmbientParams( AmbientConfig* ambient_config, QWidget* parent )
	: QDialog( parent ),
	  ambient_config( ambient_config ) {

	ui.setupUi( this );

	ui.doubleSpinBox_Ambient_r->setValue( ambient_config->amb_r );
	ui.doubleSpinBox_Ambient_g->setValue( ambient_config->amb_g );
	ui.doubleSpinBox_Ambient_b->setValue( ambient_config->amb_b );

	ui.doubleSpinBox_radianceScalingFactor->setValue( ambient_config->radiance_scaling_factor );
}

SetAmbientParams::~SetAmbientParams( ) = default;


void SetAmbientParams::on_pushButton_Cancel_Ambient_clicked( ) {
	close( );
}


void SetAmbientParams::on_pushButton_Confirm_Ambient_clicked( ) {
	ambient_config->set_ambient_color(ui.doubleSpinBox_Ambient_r->value(), ui.doubleSpinBox_Ambient_g->value(), ui.doubleSpinBox_Ambient_b->value());
	ambient_config->radiance_scaling_factor = ui.doubleSpinBox_radianceScalingFactor->value();
	close( );
}
