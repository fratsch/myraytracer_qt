#pragma once


enum class Samplers {
	REG ,
	RAND ,
	NR ,
	JIT ,
	MULTI_JIT ,
	HAM
};


class SamplerConfig {

public:

	SamplerConfig( );
	~SamplerConfig( );

	// sampler type
	Samplers sampler;

	// number of samples per set
	int num_samples;

	// number of sample sets
	int num_sets;

};
