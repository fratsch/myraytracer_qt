#pragma once

#include "ui_SetCameraParams.h"
#include "RayTracerGUI.h"


class CameraConfig;


class SetCameraParams : public QDialog {

Q_OBJECT

public:

	SetCameraParams( CameraConfig* camera_config, QWidget* parent = Q_NULLPTR );
	~SetCameraParams( );

public slots:

	void on_pushButton_Cancel_Camera_clicked( );

	void on_pushButton_Confirm_Camera_clicked( );

private:

	Ui::SetCameraParams ui;

	CameraConfig* camera_config;
};