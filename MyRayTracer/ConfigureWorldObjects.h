#pragma once


#include "ui_ConfigureWorldObjects.h"


class GeometricObject;
class WorldConfig;


class ConfigureWorldObjects : public QDialog {

Q_OBJECT

public:

	ConfigureWorldObjects( WorldConfig* world_config, QWidget* parent = Q_NULLPTR );
	~ConfigureWorldObjects( );

	void add_WorldObject( GeometricObject* object, QString object_name );

public slots:

	// close button action
	void on_pushButton_Close_ConfigureWorldObjects_clicked( );

	// add object button action
	void on_pushButton_AddObject_clicked( );

	// delete object button action
	void on_pushButton_DeleteObject_clicked( );

	// edit object button action
	void on_pushButton_EditObject_clicked( );

private:

	Ui::ConfigureWorldObjects ui;

	WorldConfig* world_config;
};
