#pragma once

#include <QListWidgetItem>


class Material;


class MaterialEditorItem : public QListWidgetItem {

public:

	MaterialEditorItem( Material* material, QString material_name );
	~MaterialEditorItem( );

	Material* get_material( );

private:

	Material* material;

};


inline Material* MaterialEditorItem::get_material( ) {
	return material;
}
