#include "MaterialEditor.h"


#include "WorldConfig.h"
#include "Matte.h"
#include "ConfigureWorldMaterials.h"


MaterialEditor::MaterialEditor( QWidget* parent )
	: QDialog( parent ) {
	ui.setupUi( this );
}

MaterialEditor::~MaterialEditor( ) {}

void MaterialEditor::on_pushButton_Close_MaterialEditor_clicked( ) {
	close( );
}

void MaterialEditor::on_pushButton_AddEdit_Material_clicked( ) {
	Material* material;
	switch( ui.tabWidget_MaterialParameters->currentIndex( ) ) {
	case 0 :
		material = new Matte( true,
		                      new Lambertian( ui.doubleSpinBox_Ambient_kd->value( ),
		                                      RGBColor( ui.doubleSpinBox_Ambient_r->value( ), ui.doubleSpinBox_Ambient_g->value( ),
		                                                ui.doubleSpinBox_Ambient_b->value( ) ) ),
		                      new Lambertian( ui.doubleSpinBox_Diffuse_kd->value( ),
		                                      RGBColor( ui.doubleSpinBox_Diffuse_r->value( ), ui.doubleSpinBox_Diffuse_g->value( ),
		                                                ui.doubleSpinBox_Diffuse_b->value( ) ) ) );
		break;
	}
	dynamic_cast<ConfigureWorldMaterials*>( parentWidget( ) )->add_WorldMaterial( material, ui.lineEdit_materialName->text( ) );
	material = nullptr;

	close( );
}
