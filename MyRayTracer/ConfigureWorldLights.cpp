#include "ConfigureWorldLights.h"


#include "LightEditor.h"
#include "LightEditorItem.h"
#include "WorldConfig.h"


ConfigureWorldLights::ConfigureWorldLights( WorldConfig* world_config, QWidget* parent )
	: QDialog( parent ),
	  world_config( world_config ) {

	ui.setupUi( this );

	for( LightEditorItem* item : world_config->get_lights( ) ) {
		ui.listWidget_worldLights->addItem( item );
	}
}

ConfigureWorldLights::~ConfigureWorldLights( ) = default;

void ConfigureWorldLights::add_WorldLight( Light* light, QString light_name ) {
	LightEditorItem* item = new LightEditorItem( light, light_name );
	ui.listWidget_worldLights->addItem( item );
	world_config->add_light( item );
}

void ConfigureWorldLights::on_pushButton_Close_ConfigureWorldLights_clicked( ) {
	for (int i = ui.listWidget_worldLights->count() - 1; i >= 0; --i) {
		ui.listWidget_worldLights->takeItem(i);
	}
	close( );
}

void ConfigureWorldLights::on_pushButton_AddLight_clicked( ) {
	LightEditor light_editor_window( this );
	light_editor_window.setModal( true );
	light_editor_window.exec( );
}

void ConfigureWorldLights::on_pushButton_DeleteLight_clicked( ) {
	if( ui.listWidget_worldLights->currentItem( ) != nullptr ) {
		QListWidgetItem* item = ui.listWidget_worldLights->takeItem( ui.listWidget_worldLights->currentRow( ) );
		world_config->delete_light( dynamic_cast<LightEditorItem*>( item ) );
		delete item;
		item = nullptr;
	}
}

void ConfigureWorldLights::on_pushButton_EditLight_clicked( ) {}
