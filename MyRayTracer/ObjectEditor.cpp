#include "ObjectEditor.h"


#include "Plane.h"
#include "Point3D.h"
#include "Normal.h"
#include "Sphere.h"
#include "ConfigureWorldObjects.h"
#include "MaterialEditorItem.h"


ObjectEditor::ObjectEditor( ObjectEditorItem* object, QList<MaterialEditorItem*> materials, bool edit_mode, QWidget* parent )
	: QDialog( parent ),
	  materials( materials ) {
	ui.setupUi( this );

	for( MaterialEditorItem* material : ObjectEditor::materials ) {
		ui.comboBox_objectMaterial->addItem( material->text( ) );
	}
}

ObjectEditor::~ObjectEditor( ) {}

void ObjectEditor::on_pushButton_Close_ObjectEditor_clicked( ) {
	close( );
}

void ObjectEditor::on_pushButton_AddEdit_Object_clicked( ) {
	GeometricObject* object;
	switch( ui.tabWidget_ObjectParameters->currentIndex( ) ) {
	case 0 :
		object = new Plane(
			Point3D( ui.doubleSpinBox_Pivot_xPos->value( ), ui.doubleSpinBox_Pivot_yPos->value( ), ui.doubleSpinBox_Pivot_zPos->value( ) ),
			Normal( ui.doubleSpinBox_Normal_x->value( ), ui.doubleSpinBox_Normal_y->value( ), ui.doubleSpinBox_Normal_z->value( ) ) );
		break;
	case 1 :
		object = new Sphere(
			Point3D( ui.doubleSpinBox_Center_xPos->value( ), ui.doubleSpinBox_Center_yPos->value( ),
			         ui.doubleSpinBox_Center_zPos->value( ) ), ui.doubleSpinBox_Radius->value( ) );
		break;
	}
	object->set_material(materials[ui.comboBox_objectMaterial->currentIndex()]->get_material());
	dynamic_cast<ConfigureWorldObjects*>( parentWidget( ) )->add_WorldObject( object, ui.lineEdit_objectName->text( ) );
	object = nullptr;

	close( );
}

void ObjectEditor::read_object_parameters( ObjectEditorItem* object ) {}
