#include "ConfigureWorldObjects.h"


#include <QDebug>


#include "ObjectEditor.h"
#include "ObjectEditorItem.h"
#include "WorldConfig.h"


ConfigureWorldObjects::ConfigureWorldObjects( WorldConfig* world_config, QWidget* parent )
	: QDialog( parent ),
	  world_config( world_config ) {

	ui.setupUi( this );

	for( ObjectEditorItem* item : world_config->get_objects( ) ) {
		ui.listWidget_worldObjects->addItem( item );
	}
}

ConfigureWorldObjects::~ConfigureWorldObjects( ) = default;

void ConfigureWorldObjects::add_WorldObject( GeometricObject* object, QString object_name ) {
	ObjectEditorItem* item = new ObjectEditorItem( object, object_name );
	ui.listWidget_worldObjects->addItem( item );
	world_config->add_object( item );
}

void ConfigureWorldObjects::on_pushButton_Close_ConfigureWorldObjects_clicked( ) {
	for( int i = ui.listWidget_worldObjects->count( ) - 1 ; i >= 0 ; --i ) {
		ui.listWidget_worldObjects->takeItem( i );
	}
	close( );
}

void ConfigureWorldObjects::on_pushButton_AddObject_clicked( ) {
	ObjectEditor object_editor_window( nullptr, world_config->get_materials(  ), false, this );
	object_editor_window.setModal( true );
	object_editor_window.exec( );
}

void ConfigureWorldObjects::on_pushButton_DeleteObject_clicked( ) {
	if( ui.listWidget_worldObjects->currentItem( ) != nullptr ) {
		QListWidgetItem* item = ui.listWidget_worldObjects->takeItem( ui.listWidget_worldObjects->currentRow( ) );
		world_config->delete_object( dynamic_cast<ObjectEditorItem*>( item ) );
		delete item;
		item = nullptr;
	}
}

void ConfigureWorldObjects::on_pushButton_EditObject_clicked( ) { }
