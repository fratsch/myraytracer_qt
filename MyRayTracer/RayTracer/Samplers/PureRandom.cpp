#include "PureRandom.h"

#include "Maths.h"


PureRandom::PureRandom( )
	: Sampler( ) {}


PureRandom::PureRandom( const int samples )
	: Sampler( samples ) {
	generate_samples( );
}


PureRandom::PureRandom( const int samples, const int sets )
	: Sampler( samples, sets ) {
	generate_samples( );
}


PureRandom::PureRandom( const PureRandom& random )
	: Sampler( random ) {
	generate_samples( );
}


PureRandom::~PureRandom( ) = default;


PureRandom& PureRandom::operator=( const PureRandom& rhs ) {
	if( this == &rhs )
		return *this;

	Sampler::operator=( rhs );

	return *this;
}


void PureRandom::generate_samples( ) {
	for( int p = 0 ; p < num_sets ; p++ )
		for( int q = 0 ; q < num_samples ; q++ )
			samples.emplace_back( rand_float( ), rand_float( ) );
}
