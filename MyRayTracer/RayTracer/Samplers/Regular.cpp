#include "Regular.h"

#include <cmath>


Regular::Regular( )
	: Sampler( ) {}


Regular::Regular( const int samples )
	: Sampler( samples ) {
	generate_samples( );
}


Regular::Regular( const int samples, const int sets )
	: Sampler( samples, sets ) {
	generate_samples( );
}


Regular::Regular( const Regular& regular )
	: Sampler( regular ) {
	generate_samples( );
}


Regular::~Regular( ) = default;


Regular& Regular::operator=( const Regular& rhs ) {
	if( this == &rhs )
		return *this;

	Sampler::operator=( rhs );

	return *this;
}


void Regular::generate_samples( ) {
	const int n = static_cast<int>( std::sqrt( static_cast<float>( num_samples ) ) );

	for( int j = 0 ; j < num_sets ; j++ )
		for( int p = 0 ; p < n ; p++ )
			for( int q = 0 ; q < n ; q++ )
				samples.emplace_back( ( q + 0.5 ) / n, ( p + 0.5 ) / n );
}
