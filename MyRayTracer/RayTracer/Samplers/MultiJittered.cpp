#include "MultiJittered.h"
#include "Maths.h"


MultiJittered::MultiJittered( )
	: Sampler( ) {}


MultiJittered::MultiJittered( const int samples )
	: Sampler( samples ) {
	generate_samples( );
}


MultiJittered::MultiJittered( const int samples, const int sets )
	: Sampler( samples, sets ) {
	generate_samples( );
}


MultiJittered::MultiJittered( const MultiJittered& multi_jittered )
	: Sampler( multi_jittered ) {
	generate_samples( );
}


MultiJittered& MultiJittered::operator=( const MultiJittered& rhs ) {
	if( this == &rhs )
		return *this;

	Sampler::operator=( rhs );

	return *this;
}


MultiJittered::~MultiJittered( ) = default;


void MultiJittered::generate_samples( ) {

	const int n = static_cast<int>( sqrt( static_cast<double>( num_samples ) ) );
	const float subcell_width = 1.0f / static_cast<float>( num_samples );

	const Point2D fill_point;
	for( int i = 0 ; i < num_samples * num_sets ; i++ )
		samples.push_back( fill_point );

	for( int p = 0 ; p < num_sets ; p++ )
		for( int i = 0 ; i < n ; i++ )
			for( int j = 0 ; j < n ; j++ ) {
				samples[i * n + j + p * num_samples].x = static_cast<double>( ( i * n + j ) * subcell_width + rand_float( 0, subcell_width ) );
				samples[i * n + j + p * num_samples].y = static_cast<double>( ( i * n + j ) * subcell_width + rand_float( 0, subcell_width ) );
			}

	for( int p = 0 ; p < num_sets ; p++ )
		for( int i = 0 ; i < n ; i++ )
			for( int j = 0 ; j < n ; j++ ) {
				const int k = rand_int( j, n - 1 );
				const double temp = samples[i * n + j + p * num_samples].x;
				samples[i * n + j + p * num_samples].x = samples[i * n + k + p * num_samples].x;
				samples[i * n + k + p * num_samples].x = temp;
			}

	for( int p = 0 ; p < num_sets ; p++ )
		for( int i = 0 ; i < n ; i++ )
			for( int j = 0 ; j < n ; j++ ) {
				const int k = rand_int( j, n - 1 );
				const double temp = samples[i * n + j + p * num_samples].y;
				samples[j * n + i + p * num_samples].y = samples[k * n + i + p * num_samples].y;
				samples[k * n + i + p * num_samples].y = temp;
			}
}
