#pragma once

#include "Sampler.h"


class Hammersley final : public Sampler {

public:

	Hammersley( );
	explicit Hammersley( int samples );
	Hammersley( int samples, int sets );
	Hammersley( const Hammersley& hammersley );
	virtual ~Hammersley( );

	Hammersley& operator=( const Hammersley& rhs );

private:

	static double phi( int j );

	void generate_samples( ) override;

};
