#include "NRooks.h"

#include "Maths.h"


NRooks::NRooks( )
	: Sampler( ) { }


NRooks::NRooks( const int samples )
	: Sampler( samples ) {
	generate_samples( );
}


NRooks::NRooks( const int samples, const int sets )
	: Sampler( samples, sets ) {
	generate_samples( );
}


NRooks::NRooks( const NRooks& nrooks )
	: Sampler( nrooks ) {
	generate_samples( );
}


NRooks::~NRooks( ) = default;


NRooks& NRooks::operator=( const NRooks& rhs ) {
	if( this == &rhs )
		return *this;

	Sampler::operator=( rhs );

	return *this;
}


void NRooks::generate_samples( ) {
	for( int i = 0 ; i < num_sets ; i++ )
		for( int j = 0 ; j < num_samples ; j++ ) {
			Point2D sp( ( static_cast<float>( j ) + rand_float( ) ) / static_cast<float>( num_samples ),
			            ( static_cast<float>( j ) + rand_float( ) ) / static_cast<float>( num_samples ) );
			samples.push_back( sp );
		}

	shuffle_x_coordinates( );
	shuffle_y_coordinates( );
}
