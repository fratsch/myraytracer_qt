#include "Jittered.h"
#include "Maths.h"


Jittered::Jittered( )
	: Sampler( ) {}


Jittered::Jittered( const int samples )
	: Sampler( samples ) {
	generate_samples( );
}


Jittered::Jittered( const int samples, const int sets )
	: Sampler( samples, sets ) {
	generate_samples( );
}


Jittered::Jittered( const Jittered& jittered )
	: Sampler( jittered ) {
	generate_samples( );
}


Jittered::~Jittered( ) = default;


Jittered& Jittered::operator=( const Jittered& rhs ) {
	if( this == &rhs )
		return *this;

	Sampler::operator=( rhs );

	return *this;
}


void Jittered::generate_samples( ) {
	const int n = static_cast<int>( sqrt( static_cast<double>( num_samples ) ) );

	for( int i = 0 ; i < num_sets ; i++ )
		for( int j = 0 ; j < n ; j++ )
			for( int k = 0 ; k < n ; k++ ) {
				Point2D sp( ( static_cast<float>( k ) + rand_float( ) ) / static_cast<float>( n ),
				            ( static_cast<float>( j ) + rand_float( ) ) / static_cast<float>( n ) );
				samples.push_back( sp );
			}
}
