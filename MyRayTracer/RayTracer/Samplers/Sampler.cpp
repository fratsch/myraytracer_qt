#include "Sampler.h"

#include <algorithm>
#include <random>
#include <chrono>
#include "Maths.h"
#include "Constants.h"


Sampler::Sampler( )
	: num_samples( 1 ),
	  num_sets( 83 ) {
	samples.reserve( num_samples * num_sets );
	setup_shuffled_indices( );
}


Sampler::Sampler( const int samples )
	: num_samples( samples ),
	  num_sets( 83 ) {
	Sampler::samples.reserve( num_samples * num_sets );
	setup_shuffled_indices( );
}


Sampler::Sampler( const int samples, const int sets )
	: num_samples( samples ),
	  num_sets( sets ) {
	Sampler::samples.reserve( num_samples * num_sets );
	setup_shuffled_indices( );
}


Sampler::Sampler( const Sampler& sampler ) = default;


Sampler::~Sampler( ) = default;


Sampler& Sampler::operator=( const Sampler& rhs ) {
	if( this == &rhs ) {
		return *this;
	}

	// @formatter:off
	num_samples						= rhs.num_samples;
	num_sets						= rhs.num_sets;
	samples							= rhs.samples;
	shuffled_indices				= rhs.shuffled_indices;
	disk_samples					= rhs.disk_samples;
	hemisphere_samples				= rhs.hemisphere_samples;
	sphere_samples					= rhs.sphere_samples;
	count							= rhs.count;
	jump							= rhs.jump;
	// @formatter:on

	return *this;
}


void Sampler::shuffle_x_coordinates( ) {
	for( int p = 0 ; p < num_sets ; p++ )
		for( int i = 0 ; i < num_samples - 1 ; i++ ) {
			const int target = rand_int( ) % num_samples + p * num_samples;
			const double temp = samples[i + p * num_samples + 1].x;
			samples[i + p * num_samples + 1].x = samples[target].x;
			samples[target].x = temp;
		}
}


void Sampler::shuffle_y_coordinates( ) {
	for( int p = 0 ; p < num_sets ; p++ )
		for( int i = 0 ; i < num_samples - 1 ; i++ ) {
			const int target = rand_int( ) % num_samples + p * num_samples;
			const double temp = samples[i + p * num_samples + 1].y;
			samples[i + p * num_samples + 1].y = samples[target].y;
			samples[target].y = temp;
		}
}


void Sampler::setup_shuffled_indices( ) {
	shuffled_indices.reserve( num_samples * num_sets );
	std::vector<int> indices;

	indices.reserve( num_samples );

	for( int i = 0 ; i < num_samples ; i++ )
		indices.push_back( i );

	const unsigned seed = std::chrono::system_clock::now( ).time_since_epoch( ).count( );
	auto rng = std::default_random_engine( seed );

	for( int i = 0 ; i < num_sets ; i++ ) {
		std::shuffle( indices.begin( ), indices.end( ), rng );

		for( int j = 0 ; j < num_samples ; j++ )
			shuffled_indices.push_back( indices[j] );
	}
}


void Sampler::map_samples_to_unit_disk( ) {
	const int size = samples.size( );
	float r, phi;
	Point2D sp;

	disk_samples.resize( size );
	disk_samples.reserve( size );

	for( int i = 0 ; i < size ; i++ ) {
		sp.x = 2.0 * samples[i].x - 1.0;
		sp.y = 2.0 * samples[i].y - 1.0;

		if( sp.x > -sp.y ) {
			if( sp.x > sp.y ) {
				r = static_cast<float>( sp.x );
				phi = static_cast<float>( sp.y / sp.x );
			}
			else {
				r = static_cast<float>( sp.y );
				phi = static_cast<float>( 2 - sp.x / sp.y );
			}
		}
		else {
			if( sp.x < sp.y ) {
				r = static_cast<float>( -sp.x );
				phi = static_cast<float>( 4 + sp.y / sp.x );
			}
			else {
				r = static_cast<float>( -sp.y );
				if( sp.y != 0.0 )
					phi = static_cast<float>( 6 - sp.x / sp.y );
				else
					phi = 0.0;
			}
		}

		phi *= static_cast<float>( PI / 4.0 );

		disk_samples[i].x = r * cos( static_cast<double>( phi ) );
		disk_samples[i].y = r * sin( static_cast<double>( phi ) );
	}

	samples.erase( samples.begin( ), samples.end( ) );
}


void Sampler::map_samples_to_hemisphere( const float exp ) {
	const int size = samples.size( );
	hemisphere_samples.reserve( num_samples * num_sets );

	for( int i = 0 ; i < size ; i++ ) {
		const float cos_phi = static_cast<float>( cos( 2.0 * PI * samples[i].x ) );
		const float sin_phi = static_cast<float>( sin( 2.0 * PI * samples[i].x ) );

		const float cos_theta = static_cast<float>( pow( 1.0 - samples[i].y, 1.0 / ( exp + 1.0 ) ) );
		const float sin_theta = static_cast<float>( sqrt( 1.0 - static_cast<double>( cos_theta ) * cos_theta ) );

		const float pu = sin_theta * cos_phi;
		const float pv = sin_theta * sin_phi;
		const float pw = cos_theta;

		hemisphere_samples.emplace_back( pu, pv, pw );
	}
}


void Sampler::map_samples_to_sphere( ) {

	sphere_samples.reserve( num_samples * num_sets );

	for( int i = 0 ; i < num_samples * num_sets ; i++ ) {
		const float r1 = static_cast<float>( samples[i].x );
		const float r2 = static_cast<float>( samples[i].y );

		const float z = static_cast<float>( 1.0 - 2.0 * r1 );
		const float r = static_cast<float>( sqrt( 1.0 - static_cast<double>( z ) * z ) );
		const float phi = static_cast<float>( TWO_PI * r2 );

		const float x = r * cos( phi );
		const float y = r * sin( phi );

		sphere_samples.emplace_back( x, y, z );
	}
}


Point2D Sampler::sample_unit_square( ) {
	if( count % num_samples == 0 )  									// start of a new pixel
		jump = rand_int( ) % num_sets * num_samples;				// random index jump initialised to zero in constructor

	return samples[jump + shuffled_indices[jump + count++ % num_samples]];
}


Point2D Sampler::sample_unit_disk( ) {
	if( count % num_samples == 0 )
		jump = rand_int( ) % num_sets * num_samples;

	return disk_samples[jump + shuffled_indices[jump + count++ % num_samples]];
}


Point3D Sampler::sample_hemisphere( ) {
	if( count % num_samples == 0 )
		jump = rand_int( ) % num_sets * num_samples;

	return hemisphere_samples[jump + shuffled_indices[jump + count++ % num_samples]];
}


Point3D Sampler::sample_sphere( ) {
	if( count % num_samples == 0 )
		jump = rand_int( ) % num_sets * num_samples;

	return sphere_samples[jump + shuffled_indices[jump + count++ % num_samples]];
}


Point2D Sampler::sample_one_set( ) {
	return samples[count++ % num_samples];
}
