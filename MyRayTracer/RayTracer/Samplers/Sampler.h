#pragma once

#include <vector>
#include "Point2D.h"
#include "Point3D.h"


class Sampler {

public:

	Sampler( );
	explicit Sampler( int samples );
	Sampler( int samples, int sets );
	Sampler( const Sampler& sampler );
	virtual ~Sampler( );

	Sampler& operator=( const Sampler& rhs );

	void setup_shuffled_indices( );

	Point2D sample_unit_square( );

	Point2D sample_unit_disk( );

	Point3D sample_hemisphere( );

	Point3D sample_sphere( );

	Point2D sample_one_set( );

	int get_num_samples( ) const;

	void set_num_sets( int num );

	void shuffle_x_coordinates( );

	void shuffle_y_coordinates( );

	void map_samples_to_unit_disk( );

	void map_samples_to_hemisphere( float exp );

	void map_samples_to_sphere( );

protected:
	// @formatter:off
	int							num_samples{};				
	int							num_sets{};					
	std::vector<Point2D>		samples;					
	std::vector<int>			shuffled_indices;			
	std::vector<Point2D>		disk_samples;				
	std::vector<Point3D>		hemisphere_samples;			
	std::vector<Point3D>		sphere_samples;				
	unsigned long				count{};						
	int							jump{};						
	// @formatter:on

	virtual void generate_samples( ) = 0;

};


inline void Sampler::set_num_sets( const int num ) {
	num_sets = num;
}

inline int Sampler::get_num_samples( ) const {
	return num_samples;
}
