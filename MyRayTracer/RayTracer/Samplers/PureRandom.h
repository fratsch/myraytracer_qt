#pragma once

#include "Sampler.h"


class PureRandom final : public Sampler {

public:

	PureRandom( );
	explicit PureRandom( int samples );
	PureRandom(int samples, int sets);
	PureRandom( const PureRandom& random );
	virtual ~PureRandom( );

	PureRandom& operator=( const PureRandom& rhs );

private:

	void generate_samples( ) override;
};
