#pragma once

#include "Sampler.h"


class MultiJittered final : public Sampler {

public:

	MultiJittered( );
	explicit MultiJittered( int samples );
	MultiJittered( int samples, int sets );
	MultiJittered( const MultiJittered& multi_jittered );
	virtual ~MultiJittered( );

	MultiJittered& operator=( const MultiJittered& rhs );

private:

	void generate_samples( ) override;

};
