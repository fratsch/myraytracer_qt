#pragma once

#include "Sampler.h"


class Jittered final : public Sampler {

public:

	Jittered( );
	explicit Jittered( int samples );
	Jittered( int samples, int sets );
	Jittered( const Jittered& jittered );
	virtual ~Jittered( );

	Jittered& operator=( const Jittered& rhs );

private:

	void generate_samples( ) override;
};
