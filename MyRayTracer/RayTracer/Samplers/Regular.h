#pragma once

#include "Sampler.h"


class Regular final : public Sampler {

public:

	Regular( );
	explicit Regular( int samples );
	Regular(int samples, int sets);
	Regular( const Regular& regular );
	virtual ~Regular( );

	Regular& operator=( const Regular& rhs );

private:

	void generate_samples() override;
	
};
