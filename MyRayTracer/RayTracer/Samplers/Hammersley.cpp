#include "Hammersley.h"


Hammersley::Hammersley( )
	: Sampler( ) {}


Hammersley::Hammersley( const int samples )
	: Sampler( samples ) {
	generate_samples( );
}


Hammersley::Hammersley( const int samples, const int sets )
	: Sampler( samples, sets ) {
	generate_samples( );
}


Hammersley::Hammersley( const Hammersley& h )
	: Sampler( h ) {
	generate_samples( );
}


Hammersley::~Hammersley( ) = default;


Hammersley& Hammersley::operator=( const Hammersley& rhs ) {
	if( this == &rhs )
		return *this;

	Sampler::operator=( rhs );

	return *this;
}


double Hammersley::phi( int j ) {
	double x = 0.0;
	double f = 0.5;

	while( j ) {
		x += f * static_cast<double>( j % 2 );
		j /= 2;
		f *= 0.5;
	}

	return x;
}


void Hammersley::generate_samples( ) {
	for( int p = 0 ; p < num_sets ; p++ )
		for( int j = 0 ; j < num_samples ; j++ ) {
			Point2D pv( ( static_cast<double>( j ) / static_cast<double>( num_samples ), phi( j ) ) );
			samples.push_back( pv );
		}
}
