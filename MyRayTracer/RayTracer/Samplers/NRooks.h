#pragma once

#include "Sampler.h"


class NRooks final : public Sampler {

public:

	NRooks( );
	explicit NRooks( int samples );
	NRooks( int samples, int sets );
	NRooks( const NRooks& nrooks );
	virtual ~NRooks( );

	NRooks& operator=( const NRooks& rhs );

private:

	void generate_samples() override;
};
