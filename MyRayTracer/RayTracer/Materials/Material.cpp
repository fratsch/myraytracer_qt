#include "Material.h"

#include "Constants.h"


Material::Material( )
	: shadows( true ) {}


Material::Material( const bool shadows )
	: shadows( shadows ) { }


Material::Material( const Material& material ) {
	shadows = material.shadows;
}


Material::~Material( ) = default;


Material& Material::operator=( const Material& rhs ) {
	if( this == &rhs )
		return *this;

	shadows = rhs.shadows;

	return *this;
}


RGBColor Material::shade( ShadeRec& sr ) {
	return black;
}


RGBColor Material::area_light_shade( ShadeRec& sr ) {
	return black;
}


RGBColor Material::path_shade( ShadeRec& sr ) {
	return black;
}
