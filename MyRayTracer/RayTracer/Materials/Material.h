#pragma once

#include "RGBColor.h"

class ShadeRec;


class Material {

public:

	Material( );
	explicit Material( bool shadows );
	Material( const Material& material );
	virtual ~Material( );

	virtual RGBColor shade( ShadeRec& sr );

	virtual RGBColor area_light_shade( ShadeRec& sr );

	virtual RGBColor path_shade( ShadeRec& sr );

protected:

	Material& operator=( const Material& rhs );

	bool shadows;

};
