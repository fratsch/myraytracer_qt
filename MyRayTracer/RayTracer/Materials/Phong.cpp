#include "Phong.h"

#include "ShadeRec.h"
#include "World.h"
#include "Lambertian.h"
#include "GlossySpecular.h"
#include "Light.h"


Phong::Phong( )
	: Material( true ),
	  ambient_brdf( new Lambertian ),
	  diffuse_brdf( new Lambertian ),
	  specular_brdf( new GlossySpecular ) { }


Phong::Phong( const Phong& phong )
	: Material( phong ) {
	if( phong.ambient_brdf )
		ambient_brdf = phong.ambient_brdf->clone( );
	else
		ambient_brdf = nullptr;

	if( phong.diffuse_brdf )
		diffuse_brdf = phong.diffuse_brdf->clone( );
	else
		diffuse_brdf = nullptr;

	if( phong.specular_brdf )
		specular_brdf = phong.specular_brdf->clone( );
	else
		specular_brdf = nullptr;
}


Phong::Phong( const bool shadows, Lambertian* ambient_brdf, Lambertian* diffuse_brdf, GlossySpecular* specular_brdf )
	: Material( shadows ),
	  ambient_brdf( ambient_brdf ),
	  diffuse_brdf( diffuse_brdf ),
	  specular_brdf( specular_brdf ) { }


Phong::~Phong( ) {
	if( ambient_brdf ) {
		delete ambient_brdf;
		ambient_brdf = nullptr;
	}

	if( diffuse_brdf ) {
		delete diffuse_brdf;
		diffuse_brdf = nullptr;
	}
	if( specular_brdf ) {
		delete specular_brdf;
		specular_brdf = nullptr;
	}
}


Phong& Phong::operator=( const Phong& rhs ) {

	if( this == &rhs )
		return *this;

	Material::operator=( rhs );

	if( ambient_brdf ) {
		delete ambient_brdf;
		ambient_brdf = nullptr;
	}

	if( rhs.ambient_brdf )
		ambient_brdf = rhs.ambient_brdf->clone( );

	if( diffuse_brdf ) {
		delete diffuse_brdf;
		diffuse_brdf = nullptr;
	}

	if( rhs.diffuse_brdf )
		diffuse_brdf = rhs.diffuse_brdf->clone( );

	if( specular_brdf ) {
		delete specular_brdf;
		specular_brdf = nullptr;
	}

	if( rhs.specular_brdf )
		specular_brdf = rhs.specular_brdf->clone( );

	return *this;
}


RGBColor Phong::shade( ShadeRec& sr ) {
	const Vector3D wo = -sr.ray.d;
	RGBColor L = ambient_brdf->rho( sr, wo ) * sr.w.ambient_ptr->L( sr );
	const int num_lights = sr.w.lights.size( );

	for( int j = 0 ; j < num_lights ; ++j ) {
		Vector3D wi = sr.w.lights[j]->get_direction( sr );
		const float ndotwi = static_cast<float>(sr.normal * wi);

		if( ndotwi > 0.0f ) {
			bool in_shadow = false;

			if( shadows && sr.w.lights[j]->casts_shadows( ) ) {
				Ray shadowRay( sr.hit_point, wi );
				in_shadow = sr.w.lights[j]->in_shadow( shadowRay, sr );
			}

			if( !in_shadow )
				L += ( diffuse_brdf->f( sr, wo, wi ) + specular_brdf->f( sr, wo, wi ) ) * sr.w.lights[j]->L( sr ) * ndotwi;
		}
	}

	return L;
}
