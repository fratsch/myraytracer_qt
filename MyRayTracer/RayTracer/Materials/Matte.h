#pragma once

#include "Material.h"

#include "Lambertian.h"


class Matte final : public Material {

public:

	Matte( );
	Matte( const Matte& matte );
	Matte( bool shadows, Lambertian* ambient_brdf, Lambertian* diffuse_brdf );
	~Matte( );

	Matte& operator=( const Matte& rhs );

	RGBColor shade( ShadeRec& sr ) override;

	void set_ka( float ka ) const;

	void set_kd( float kd ) const;

	void set_cd( const RGBColor& c ) const;

	void set_cd( float r, float g, float b ) const;

	void set_cd( float c ) const;

private:

	Lambertian* ambient_brdf;
	Lambertian* diffuse_brdf;

};


inline void Matte::set_ka( const float ka ) const {
	ambient_brdf->set_kd( ka );
}


inline void Matte::set_kd( const float kd ) const {
	diffuse_brdf->set_kd( kd );
}


inline void Matte::set_cd( const RGBColor& c ) const {
	ambient_brdf->set_cd( c );
	diffuse_brdf->set_cd( c );
}


inline void Matte::set_cd( const float r, const float g, const float b ) const {
	ambient_brdf->set_cd( r, g, b );
	diffuse_brdf->set_cd( r, g, b );
}


inline void Matte::set_cd( const float c ) const {
	ambient_brdf->set_cd( c );
	diffuse_brdf->set_cd( c );
}
