#pragma once

#include "Material.h"

#include "Lambertian.h"
#include "GlossySpecular.h"


class Phong final : public Material {

public:

	Phong( );
	Phong( const Phong& phong );
	Phong( bool shadows, Lambertian* ambient_brdf, Lambertian* diffuse_brdf, GlossySpecular* specular_brdf );
	~Phong( );

	Phong& operator=( const Phong& rhs );

	RGBColor shade( ShadeRec& sr ) override;

	void set_ka(float ka) const;

	void set_kd(float kd) const;

	void set_ks(float ks) const;

	void set_cd(const RGBColor& c) const;

	void set_cd(float r, float g, float b) const;

	void set_cd(float c) const;

protected:

	Lambertian* ambient_brdf;
	Lambertian* diffuse_brdf;
	GlossySpecular* specular_brdf;
	
};


inline void Phong::set_ka( const float ka ) const {
	ambient_brdf->set_kd( ka );
}


inline void Phong::set_kd( const float kd ) const {
	diffuse_brdf->set_kd( kd );
}


inline void Phong::set_ks( const float ks ) const {
	specular_brdf->set_ks( ks );
}


inline void Phong::set_cd( const RGBColor& c ) const {
	ambient_brdf->set_cd( c );
	diffuse_brdf->set_cd( c );
	specular_brdf->set_cs( c );
}


inline void Phong::set_cd( const float r, const float g, const float b ) const {
	ambient_brdf->set_cd( r, g, b );
	diffuse_brdf->set_cd( r, g, b );
	specular_brdf->set_cs( r, g, b );
}


inline void Phong::set_cd( const float c ) const {
	ambient_brdf->set_cd( c );
	diffuse_brdf->set_cd( c );
	specular_brdf->set_cs( c );
}
