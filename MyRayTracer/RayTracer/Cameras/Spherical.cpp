#include "Spherical.h"

#include <cmath>

#include "Point2D.h"
#include "Constants.h"
#include "ViewPlane.h"
#include "World.h"


// ----------------------------------------------------------------------------- default constructor
Spherical::Spherical( )
	: Camera( ),
	  lambda_max( 180.0f ),
	  psi_max( 180.0f ) {}


// ----------------------------------------------------------------------------- copy constructor
Spherical::Spherical( const Spherical& spherical )
	: Camera( spherical ),
	  lambda_max( spherical.lambda_max ),
	  psi_max( spherical.psi_max ) {}


// ----------------------------------------------------------------------------- clone
Camera* Spherical::clone( ) const {
	return new Spherical( *this );
}


// ----------------------------------------------------------------------------- assignment operator
Spherical& Spherical::operator=( const Spherical& rhs ) {
	if( this == &rhs )
		return *this;

	Camera::operator=( rhs );

	lambda_max = rhs.lambda_max;
	psi_max = rhs.psi_max;

	return *this;
}


// ----------------------------------------------------------------------------- destructor
Spherical::~Spherical( ) = default;


// ----------------------------------------------------------------------------- ray_direction
Vector3D Spherical::ray_direction( const Point2D& pp, const int hres, const int vres, const float s ) const {

	// compute normalized device coordinates
	Point2D pn( 2.0f / ( s * hres ) * pp.x, 2.0f / ( s * vres ) * pp.y );

	// compute angles lambda and phi in radians
	float lambda = pn.x * lambda_max * PI_ON_180;
	float psi = pn.y * psi_max * PI_ON_180;

	// compute spherical azimuth and polar angles
	// @formatter:off
	float phi			= PI - lambda;
	float theta			= 0.5f * PI - psi;
	float sin_phi		= sin( phi );
	float cos_phi		= cos( phi );
	float sin_theta		= sin( theta );
	float cos_theta		= cos( theta );
	// @formatter:on

	Vector3D dir = sin_theta * sin_phi * u + cos_theta * sin_theta * v + cos_phi * w;

	return dir;
}


// ----------------------------------------------------------------------------- render_scene
void Spherical::render_scene( World& world ) {
	RGBColor L;
	ViewPlane vp( world.vp );
	int hres = vp.hres;
	int vres = vp.vres;
	float s = vp.s;
	Ray ray;
	int depth = 0;
	Point2D sp;
	Point2D pp;

	ray.o = eye;

	world.pixel_output.createImage( vp.hres, vp.vres );

	for( int r = 0 ; r < vres ; r++ )
		for( int c = 0 ; c < hres ; c++ ) {
			L = black;

			for( int j = 0 ; j < vp.num_samples ; j++ ) {
				sp = vp.sampler_ptr->sample_unit_square( );
				pp.x = s * ( c - 0.5 * hres + sp.x );
				pp.y = s * ( r - 0.5 * vres + sp.y );

				ray.d = ray_direction( pp, hres, vres, s );
			}

			L /= vp.num_samples;
			L *= exposure_time;

			world.display_pixel( r, c, L );
		}
}
