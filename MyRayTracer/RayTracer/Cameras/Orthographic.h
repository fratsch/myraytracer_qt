#pragma once

#include "Camera.h"


class Orthographic final : public Camera {

public:

	Orthographic( );
	Orthographic( const Orthographic& orthographic );
	virtual ~Orthographic( );

	Orthographic& operator=( const Orthographic& rhs );

	Vector3D get_direction( ) const;

	void render_scene( World& world ) override;
};
