#include "Camera.h"


Camera::Camera( )
	: eye( 0.0, 0.0, 500.0 ),
	  lookat( 0.0 ),
	  ra( 0.0f ),
	  u( 1.0, 0.0, 0.0 ),
	  v( 0.0, 1.0, 0.0 ),
	  w( 0.0, 0.0, 1.0 ),
	  up( 0.0, 1.0, 0.0 ),
	  exposure_time( 1.0f ) {}


Camera::Camera( const Camera& camera ) = default;


Camera& Camera::operator=( const Camera& rhs ) {
	if( this == &rhs )
		return *this;

	// @formatter:off

	eye				= rhs.eye;
	lookat			= rhs.lookat;
	ra				= rhs.ra;
	up				= rhs.up;
	u				= rhs.u;
	v				= rhs.v;
	w				= rhs.w;
	exposure_time	= rhs.exposure_time;

	// @formatter:on

	return *this;
}


Camera::~Camera( ) = default;


void Camera::compute_uvw( ) {
	w = eye - lookat;
	w.normalize( );
	u = up ^ w;
	u.normalize( );
	v = w ^ u;
	
	if( eye.x == lookat.x && eye.z == lookat.z && eye.y > lookat.y ) { // camera is looking straight down
		u = Vector3D( 1.0, 0.0, 0.0 );
		v = Vector3D( 0.0, 0.0, -1.0 );
		w = Vector3D( 0.0, 1.0, 0.0 );
	}

	if( eye.x == lookat.x && eye.z == lookat.z && eye.y < lookat.y ) { // camera looking straight up
		u = Vector3D( 1.0, 0.0, 0.0 );
		v = Vector3D( 0.0, 0.0, 1.0 );
		w = Vector3D( 0.0, -1.0, 0.0 );
	}
}
