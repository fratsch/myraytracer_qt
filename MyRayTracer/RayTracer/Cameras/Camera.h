#pragma once

#include "Vector3D.h"
#include "Point3D.h"
#include "Tracer.h"

class World;


class Camera {

public:

	Camera( );
	Camera( const Camera& camera );
	virtual ~Camera( );

	virtual void render_scene( World& world ) = 0;

	void set_eye( const Point3D& p );

	void set_eye( float x, float y, float z );

	void set_lookat( const Point3D& p );

	void set_lookat( float x, float y, float z );

	void set_up_vector( const Vector3D& up );

	void set_up_vector( float x, float y, float z );

	void set_roll( float ra );

	void set_exposure_time( float exposure );

	void compute_uvw( );

protected:
	// @formatter:off

	Point3D			eye;
	Point3D			lookat;
	float			ra{};
	Vector3D		u, v, w;
	Vector3D		up;
	float			exposure_time{};

	Camera& operator= (const Camera& rhs);

	// @formatter:on
};


inline void Camera::set_eye( const Point3D& p ) {
	eye = p;
}


inline void Camera::set_eye( const float x, const float y, const float z ) {
	eye.x = x;
	eye.y = y;
	eye.z = z;
}


inline void Camera::set_lookat( const Point3D& p ) {
	lookat = p;
}


inline void Camera::set_lookat( const float x, const float y, const float z ) {
	lookat.x = x;
	lookat.y = y;
	lookat.z = z;
}


inline void Camera::set_up_vector( const Vector3D& up ) {
	Camera::up = up;
}


inline void Camera::set_up_vector( const float x, const float y, const float z ) {
	up.x = x;
	up.y = y;
	up.z = z;
}


inline void Camera::set_roll( const float ra ) {
	Camera::ra = ra;
}


inline void Camera::set_exposure_time( const float exposure ) {
	exposure_time = exposure;
}
