#include "ThinLense.h"

#include "Sampler.h"
#include "World.h"
#include "Constants.h"


ThinLense::ThinLense( )
	: Camera( ),
	  lens_radius( 1.0f ),
	  view_distance( 500.0f ),
	  focal_distance( 500.0f ),
	  zoom( 1.0f ),
	  sampler_ptr( nullptr ) { }


ThinLense::ThinLense( const ThinLense& thinlense )
	: Camera( thinlense ),
	  lens_radius( thinlense.lens_radius ),
	  view_distance( thinlense.view_distance ),
	  focal_distance( thinlense.focal_distance ),
	  zoom( thinlense.zoom ),
	  sampler_ptr( thinlense.sampler_ptr ) { }


Camera* ThinLense::clone( ) const {
	return new ThinLense( *this );
}


ThinLense& ThinLense::operator=( const ThinLense& rhs ) {
	if( this == &rhs )
		return *this;

	Camera::operator=( rhs );

	lens_radius = rhs.lens_radius;
	view_distance = rhs.view_distance;
	focal_distance = rhs.focal_distance;
	zoom = rhs.zoom;
	sampler_ptr = rhs.sampler_ptr;

	return *this;
}


ThinLense::~ThinLense( ) {
	if( sampler_ptr ) {
		delete sampler_ptr;
		sampler_ptr = nullptr;
	}
}


void ThinLense::set_sampler( Sampler* sampler ) {
	if( sampler_ptr ) {
		delete sampler_ptr;
		sampler_ptr = nullptr;
	}

	sampler_ptr = sampler;
	sampler_ptr->map_samples_to_unit_disk( );
}


Vector3D ThinLense::get_direction( const Point2D& pixel_point, const Point2D& lens_point ) const {
	Point2D p;
	p.x = pixel_point.x * focal_distance / view_distance;
	p.y = pixel_point.y * focal_distance / view_distance;

	Vector3D dir = ( p.x - lens_point.x ) * u + ( p.y - lens_point.y ) * v - focal_distance * w;
	dir.normalize( );

	return dir;
}


void ThinLense::render_scene( World& world ) {
	RGBColor L;
	Ray ray;
	ViewPlane vp( world.vp );
	int depth = 0;

	Point2D sp;				// sample point on a unit square
	Point2D pp;				// sample point on a pixel
	Point2D dp;				// sample point on a unit disk
	Point2D lp;				// sample point on lens

	vp.s /= zoom;

	world.pixel_output.createImage( vp.hres, vp.vres );

	for( int r = 0 ; r < vp.vres ; r++ )			// up
		for( int c = 0 ; c < vp.hres ; c++ ) {		// across

			L = black;

			for( int j = 0 ; j < vp.num_samples ; j++ ) {	// across pixel
				sp = vp.sampler_ptr->sample_unit_square( );
				pp.x = vp.s * ( c - 0.5 * vp.hres + sp.x );
				pp.y = vp.s * ( r - 0.5 * vp.vres + sp.y );

				dp = sampler_ptr->sample_unit_disk( );
				lp = dp * lens_radius;

				ray.o = eye + lp.x * u + lp.y * v;
				ray.d = get_direction( pp, lp );

				L += world.tracer_ptr->trace_ray( ray, depth );
			}

			L /= vp.num_samples;
			L *= exposure_time;

			world.display_pixel( r, c, L );
		}
}
