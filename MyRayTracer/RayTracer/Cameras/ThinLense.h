#pragma once

#include "Camera.h"

class Point2D;
class Sampler;

class ThinLense : public Camera {

public:

	ThinLense( );
	ThinLense( const ThinLense& thinlense );
	virtual Camera* clone( ) const;
	virtual ~ThinLense( );

	ThinLense& operator=( const ThinLense& rhs );

	void set_view_distance( float vd );

	void set_zoom( float zoom_factor );

	void set_focal_distance( float fd );

	void set_lens_radius( float lr );

	void set_sampler( Sampler* sampler );

	Vector3D get_direction( const Point2D& pixel_point, const Point2D& lens_point ) const;

	virtual void render_scene( World& world );

private:

	float lens_radius;
	float view_distance;
	float focal_distance;
	float zoom;
	Sampler* sampler_ptr;
};




//-------------------------------------------------------------------------- set_vpd
inline void ThinLense::set_view_distance( const float vd ) {
	view_distance = vd;
}


//-------------------------------------------------------------------------- set_zoom
inline void ThinLense::set_zoom( const float zoom_factor ) {
	zoom = zoom_factor;
}


//-------------------------------------------------------------------------- set_zoom
inline void ThinLense::set_focal_distance( const float fd ) {
	focal_distance = fd;
}


//-------------------------------------------------------------------------- set_zoom
inline void ThinLense::set_lens_radius( const float lr ) {
	lens_radius = lr;
}
