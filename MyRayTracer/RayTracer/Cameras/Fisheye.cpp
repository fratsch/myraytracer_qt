#include "Fisheye.h"

#include <cmath>

#include "Point2D.h"
#include "Constants.h"
#include "World.h"

// ----------------------------------------------------------------------------- default constructor
Fisheye::Fisheye( )
	: Camera( ),
	  psi_max( 180.0f ) {}


// ----------------------------------------------------------------------------- copy constructor
Fisheye::Fisheye( const Fisheye& fisheye )
	: Camera( fisheye ),
	  psi_max( fisheye.psi_max ) {}


// ----------------------------------------------------------------------------- clone
Camera* Fisheye::clone( ) const {
	return new Fisheye( *this );
}


// ----------------------------------------------------------------------------- assignment operator
Fisheye& Fisheye::operator=( const Fisheye& rhs ) {
	if( this == &rhs )
		return *this;

	Camera::operator=( rhs );

	psi_max = rhs.psi_max;

	return *this;
}


// ----------------------------------------------------------------------------- destructor
Fisheye::~Fisheye( ) = default;


// ----------------------------------------------------------------------------- ray_direction
Vector3D Fisheye::ray_direction( const Point2D& pp, const int hres, const int vres, const float s, float& r_squared ) const {
	// compute normalized device coordinates

	Point2D pn( 2.0f / ( s * hres ) * pp.x, 2.0f / ( s * vres ) * pp.y );
	r_squared = pn.x * pn.x + pn.y * pn.y;

	if( r_squared <= 1.0f ) {

		// @formatter:off
		float r = sqrt( r_squared );
		float psi			= r * psi_max * PI_ON_180;
		float sin_psi		= sin( psi );
		float cos_psi		= cos( psi );
		float sin_alpha		= pn.y / r;
		float cos_alpha		= pn.x / r;
		Vector3D dir		= sin_psi * cos_alpha * u + sin_psi * sin_alpha * v - cos_psi * w;
		// @formatter:on

		return dir;
	}
	else {
		return Vector3D( 0.0 );
	}
}


// ----------------------------------------------------------------------------- render_scene
void Fisheye::render_scene( World& world ) {
	RGBColor L;
	ViewPlane vp( world.vp );
	int hres = vp.hres;
	int vres = vp.vres;
	float s = vp.s;
	Ray ray;
	int depth = 0;
	Point2D sp;
	Point2D pp;
	float r_squared;

	ray.o = eye;

	world.pixel_output.createImage( vp.hres, vp.vres );

	for( int r = 0 ; r < vres ; r++ )
		for( int c = 0 ; c < hres ; c++ ) {
			L = black;

			for( int j = 0 ; j < vp.num_samples ; j++ ) {
				sp = vp.sampler_ptr->sample_unit_square( );
				pp.x = s * ( c - 0.5 * hres + sp.x );
				pp.y = s * ( r - 0.5 * vres + sp.y );

				ray.d = ray_direction( pp, hres, vres, s, r_squared );
			}

			L /= vp.num_samples;
			L *= exposure_time;

			world.display_pixel( r, c, L );
		}
}
