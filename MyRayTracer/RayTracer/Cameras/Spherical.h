#pragma once

#include "Camera.h"


class Point2D;

class Spherical : public Camera {

public:

	Spherical( );
	Spherical( const Spherical& spherical );
	virtual Camera* clone( ) const;
	virtual ~Spherical( );

	Spherical& operator=( const Spherical& rhs );

	Vector3D ray_direction( const Point2D& pp, int hres, int vres, float s ) const;

	virtual void render_scene( World& world );

private:

	float lambda_max;
	float psi_max;

};
