#pragma once

#include "Camera.h"

class Point2D;


class Pinhole : public Camera {

public:

	Pinhole( );
	Pinhole( const Pinhole& pinhole );
	virtual ~Pinhole( );

	Pinhole& operator=( const Pinhole& rhs );

	void set_view_distance( float vd );

	void set_zoom( float zoom_factor );

	Vector3D get_direction( const Point2D& p ) const;

	void render_scene( World& world ) override;

private:

	float view_distance{ };
	float zoom{ };

};


inline void Pinhole::set_view_distance( const float vd ) {
	view_distance = vd;
}


inline void Pinhole::set_zoom( const float zoom_factor ) {
	zoom = zoom_factor;
}
