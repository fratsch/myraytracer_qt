#pragma once

#include "Camera.h"


class Point2D;

class Fisheye : public Camera {

public:

	Fisheye( );
	Fisheye( const Fisheye& fisheye );
	virtual Camera* clone( ) const;
	virtual ~Fisheye( );

	Fisheye& operator=( const Fisheye& rhs );

	Vector3D ray_direction( const Point2D& pp, int hres, int vres, float s, float& r_squared ) const;

	virtual void render_scene( World& world );

private:

	float psi_max;

};
