#include "Pinhole.h"

#include <cmath>

#include "World.h"
#include "Constants.h"


Pinhole::Pinhole( )
	: Camera( ),
	  view_distance( 500 ),
	  zoom( 1.0 ) {}


Pinhole::Pinhole( const Pinhole& pinhole ) = default;


Pinhole& Pinhole::operator=( const Pinhole& rhs ) {
	if( this == &rhs )
		return *this;

	Camera::operator=( rhs );

	view_distance = rhs.view_distance;
	zoom = rhs.zoom;

	return *this;
}


Pinhole::~Pinhole( ) = default;


Vector3D Pinhole::get_direction( const Point2D& p ) const {
	Vector3D dir = p.x * u + p.y * v - view_distance * w;
	dir.normalize( );

	return dir;
}


void Pinhole::render_scene( World& world ) {
	ViewPlane vp( world.vp );
	Ray ray;
	const int depth = 0;
	Point2D pp; // sample points on a pixel
	int n = static_cast<int>( sqrt( static_cast<float>( vp.num_samples ) ) );

	vp.s /= zoom;
	ray.o = eye;

	world.pixel_output.createImage( vp.hres, vp.vres );

	for( int r = 0 ; r < vp.vres ; r++ )			// up
		for( int c = 0 ; c < vp.hres ; c++ ) {		// across

			RGBColor L = black;

			for( int j = 0 ; j < vp.num_samples ; j++ ) {	// across pixel
				const Point2D sp = vp.sampler_ptr->sample_unit_square( );
				pp.x = vp.s * ( c - 0.5 * vp.hres + sp.x );
				pp.y = vp.s * ( r - 0.5 * vp.vres + sp.y );

				ray.d = get_direction( pp );
				L += world.tracer_ptr->trace_ray( ray, depth );
			}

			L /= static_cast<float>( vp.num_samples );
			L *= exposure_time;

			world.display_pixel( r, c, L );
		}
}
