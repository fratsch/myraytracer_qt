#include "Orthographic.h"

#include "World.h"
#include "Constants.h"


Orthographic::Orthographic( )
	: Camera( ) {}


Orthographic::Orthographic( const Orthographic& orthographic ) = default;


Orthographic& Orthographic::operator=( const Orthographic& rhs ) {
	if( this == &rhs )
		return *this;

	Camera::operator=( rhs );

	return *this;
}


Orthographic::~Orthographic( ) = default;


Vector3D Orthographic::get_direction( ) const {
	Vector3D dir = - w;
	dir.normalize( );

	return dir;
}


void Orthographic::render_scene( World& world ) {
	ViewPlane vp( world.vp );
	Ray ray;
	const int depth = 0;
	int n = static_cast<int>( sqrt( static_cast<float>( vp.num_samples ) ) );

	world.pixel_output.createImage( vp.hres, vp.vres );

	for( int r = 0 ; r < vp.vres ; r++ )			// up
		for( int c = 0 ; c < vp.hres ; c++ ) {		// across

			RGBColor L = black;

			for( int j = 0 ; j < vp.num_samples ; j++ ) {	// across pixel
				const Point2D sp = vp.sampler_ptr->sample_unit_square( );

				const Point3D pp = eye + vp.s * ( c - 0.5 * vp.hres + sp.x ) * u + vp.s * ( r - 0.5 * vp.vres + sp.y ) * v;

				ray.o = pp;
				ray.d = get_direction( );
				L += world.tracer_ptr->trace_ray( ray, depth );
			}

			L /= static_cast<float>( vp.num_samples );
			L *= exposure_time;

			world.display_pixel( r, c, L );
		}
}
