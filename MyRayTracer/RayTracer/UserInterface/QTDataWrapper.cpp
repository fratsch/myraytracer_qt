#include "QTDataWrapper.h"

QTDataWrapper::QTDataWrapper( ) {
	renderedImage = QImage( 1024, 768, QImage::Format_RGB32 );
}

QTDataWrapper::~QTDataWrapper( ) = default;

void QTDataWrapper::createImage( const int w, const int h ) {
	renderedImage = QImage( w, h, QImage::Format_RGB32 );
}


void QTDataWrapper::writePixelData( const int x, const int y, const int r, const int g, const int b ) {
	renderedImage.setPixelColor( x, y, QColor( r, g, b ) );
}
