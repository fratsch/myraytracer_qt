﻿#pragma once

#include <QImage>


class QTDataWrapper {

public:

	QImage renderedImage;

	QTDataWrapper( );
	~QTDataWrapper( );

	void createImage( int w, int h );
	
	void writePixelData( int x, int y, int r, int g, int b );
};
