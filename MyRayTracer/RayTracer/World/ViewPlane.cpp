#include "ViewPlane.h"

#include "Regular.h"
#include "PureRandom.h"
#include "NRooks.h"
#include "Jittered.h"
#include "MultiJittered.h"
#include "Hammersley.h"


ViewPlane::ViewPlane( )
	: hres( 1024 ),
	  vres( 768 ),
	  s( 1.0 ),
	  gamma( 1.0 ),
	  inv_gamma( 1.0 ),
	  show_out_of_gamut( false ),
	  max_depth( 999999.0f ),
	  sampler_ptr( nullptr ),
	  num_samples( 1 ) {}


ViewPlane::ViewPlane( const ViewPlane& vp ) = default;


ViewPlane& ViewPlane::operator=( const ViewPlane& rhs ) {
	if( this == &rhs )
		return *this;

	// @formatter:off
	hres				= rhs.hres;
	vres				= rhs.vres;
	s					= rhs.s;
	gamma				= rhs.gamma;
	inv_gamma			= rhs.inv_gamma;
	show_out_of_gamut	= rhs.show_out_of_gamut;
	max_depth			= rhs.max_depth;
	sampler_ptr			= rhs.sampler_ptr;
	num_samples			= rhs.num_samples;
	// @formatter:on

	return *this;
}


ViewPlane::~ViewPlane( ) = default;


void ViewPlane::set_sampler( const Samplers& sampler, const int samples, const int sets ) {
	if( sampler_ptr ) {
		delete sampler_ptr;
		sampler_ptr = nullptr;
	}

	num_samples = samples;

	switch( sampler ) {
	case Samplers::REG : sampler_ptr = new Regular( samples, sets );
		break;
	case Samplers::RAND : sampler_ptr = new PureRandom( samples, sets );
		break;
	case Samplers::NR : sampler_ptr = new NRooks( samples, sets );
		break;
	case Samplers::JIT : sampler_ptr = new Jittered( samples, sets );
		break;
	case Samplers::MULTI_JIT : sampler_ptr = new MultiJittered( samples, sets );
		break;
	case Samplers::HAM : sampler_ptr = new Hammersley( samples, sets );
		break;
	default : sampler_ptr = new Regular( samples, sets );
		break;
	}
}
