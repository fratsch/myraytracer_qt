#pragma once

#include <vector>

#include "ViewPlane.h"
#include "RGBColor.h"
#include "QTDataWrapper.h"
#include "ShadeRec.h"
#include "Ambient.h"

class Tracer;
class Camera;
class GeometricObject;


class World {

public:

	ViewPlane vp;
	RGBColor background_color;
	Tracer* tracer_ptr;
	Light* ambient_ptr;
	Camera* camera_ptr;

	std::vector<GeometricObject*> objects;
	std::vector<Light*> lights;

	QTDataWrapper pixel_output;

	World( );
	~World( );

	void add_object( GeometricObject* object_ptr );

	void add_light( Light* light_ptr );

	void set_ambient_light( Light* light_ptr );

	void set_sampler( const Samplers& sampler, int num_samples, int num_sets );

	void set_camera( Camera* c_ptr );

	void set_camera( int hres, int vres, float pixel_size, const Cameras& camera, const Point3D& eye, const Point3D& lookat, const Vector3D& up, float ra,
	                 float d, float z, float e, float fd, float lr );

	void set_ambient( Ambient* amb_ptr );

	void build( const std::vector<GeometricObject*>& objects, const std::vector<Light*>& lights );

	void render_scene( );

	static RGBColor max_to_one( const RGBColor& c );

	static RGBColor clamp_to_color( const RGBColor& color );

	void display_pixel( int row, int column, const RGBColor& color );

	ShadeRec hit_objects( const Ray& ray );

private:

	void build_grid( );

	void delete_objects( );

	void delete_lights( );

};


inline void World::add_object( GeometricObject* object_ptr ) {
	objects.push_back( object_ptr );
}


inline void World::add_light( Light* light_ptr ) {
	lights.push_back( light_ptr );
}


inline void World::set_ambient_light( Light* light_ptr ) {
	ambient_ptr = light_ptr;
}


inline void World::set_sampler( const Samplers& sampler, const int num_samples, const int num_sets ) {
	vp.set_sampler( sampler, num_samples, num_sets );
}


inline void World::set_camera( Camera* c_ptr ) {
	camera_ptr = c_ptr;
}


inline void World::set_ambient( Ambient* amb_ptr ) {
	ambient_ptr = static_cast<Light*>( amb_ptr );
}
