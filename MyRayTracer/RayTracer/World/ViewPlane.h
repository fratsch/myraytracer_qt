#pragma once

#include "Sampler.h"
#include "SetSamplerParams.h"


class ViewPlane {

public:

	int hres;
	int vres;
	float s;

	float gamma;
	float inv_gamma;
	bool show_out_of_gamut;

	float max_depth;

	Sampler* sampler_ptr;

	int num_samples;

	ViewPlane( );
	ViewPlane( const ViewPlane& vp );
	~ViewPlane( );

	ViewPlane& operator=( const ViewPlane& rhs );

	void set_hres( int h_res );

	void set_vres( int v_res );

	void set_pixel_size( float size );

	void set_gamma( float g );

	void set_gamut_display( bool show );

	void set_sampler( const Samplers& sampler, int samples, int sets );
	
};


inline void ViewPlane::set_hres( const int h_res ) {
	hres = h_res;
}


inline void ViewPlane::set_vres( const int v_res ) {
	vres = v_res;
}


inline void ViewPlane::set_pixel_size( const float size ) {
	s = size;
}


inline void ViewPlane::set_gamma( const float g ) {
	gamma = g;
	inv_gamma = 1.0f / gamma;
}


inline void ViewPlane::set_gamut_display( const bool show ) {
	show_out_of_gamut = show;
}
