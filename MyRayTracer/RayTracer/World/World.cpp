#include "World.h"

#include <QDebug>
#include <ctime>

#include "Constants.h"
#include "Tracer.h"
#include "Camera.h"
#include "Maths.h"
#include "GeometricObject.h"
#include "Pinhole.h"
#include "ThinLense.h"
#include "Regular.h"
#include "Orthographic.h"
#include "Ambient.h"
#include "RayCast.h"
#include "RegularGrid.h"


World::World( )
	: background_color( black ),
	  tracer_ptr( nullptr ),
	  ambient_ptr( nullptr ),
	  camera_ptr( nullptr ) {}


World::~World( ) {

	if( tracer_ptr ) {
		delete tracer_ptr;
		tracer_ptr = nullptr;
	}

	if( ambient_ptr ) {
		delete ambient_ptr;
		ambient_ptr = nullptr;
	}

	if( camera_ptr ) {
		delete camera_ptr;
		camera_ptr = nullptr;
	}

	//delete_objects( );
	//delete_lights( );
}


void World::render_scene( ) {

	qDebug( ) << "Rendering Pixels!";

	camera_ptr->render_scene( *this );

	qDebug( ) << "Pixel Rendering done!";
}


RGBColor World::max_to_one( const RGBColor& c ) {
	const float max_value = static_cast<float>( max( c.r, max( c.g, c.b ) ) );

	if( max_value > 1.0 )
		return c / max_value;

	return c;
}


RGBColor World::clamp_to_color( const RGBColor& color ) {
	RGBColor c( color );

	if( color.r > 1.0f || color.g > 1.0f || color.b > 1.0f ) {
		c.r = 1.0f;
		c.g = 0.0f;
		c.b = 0.0f;
	}

	return c;
}


void World::display_pixel( const int row, const int column, const RGBColor& color ) {
	RGBColor mapped_color;

	if( vp.show_out_of_gamut )
		mapped_color = clamp_to_color( color );
	else
		mapped_color = max_to_one( color );

	if( vp.gamma != 1.0 )
		mapped_color = mapped_color.powc( vp.inv_gamma );

	//have to start from max y coordinate to convert to screen coordinates
	const int x = column;
	const int y = vp.vres - row - 1;

	pixel_output.writePixelData( x, y, static_cast<int>( mapped_color.r * 255 ), static_cast<int>( mapped_color.g * 255 ),
	                             static_cast<int>( mapped_color.b * 255 ) );
}


ShadeRec World::hit_objects( const Ray& ray ) {
	ShadeRec sr( *this );
	double t;
	Normal normal;
	Point3D local_hit_point;
	double tmin = kHugeValue;
	const int num_objects = objects.size( );

	for( int j = 0 ; j < num_objects ; j++ )
		if( objects[j]->hit( ray, t, sr ) && ( t < tmin ) ) {
			sr.hit_an_object = true;
			tmin = t;
			sr.material_ptr = objects[j]->get_material( );
			sr.hit_point = ray.o + t * ray.d;
			normal = sr.normal;
			local_hit_point = sr.local_hit_point;
		}

	if( sr.hit_an_object ) {
		sr.t = tmin;
		sr.normal = normal;
		sr.local_hit_point = local_hit_point;
	}

	return sr;
}

void World::build_grid( ) {
	RegularGrid* grid = new RegularGrid( );

	const int num_objects = objects.size( );

	for( int i = 0 ; i < num_objects ; ++i ) {
		grid->add_object( objects[i] );
	}

	grid->setup_cells( );

	objects.clear( );
	objects.push_back( grid );
}


void World::set_camera( const int hres, const int vres, const float pixel_size, const Cameras& camera, const Point3D& eye, const Point3D& lookat,
                        const Vector3D& up, const float ra, const float d, const float z, const float e, const float fd, const float lr ) {
	vp.set_hres( hres );
	vp.set_vres( vres );

	vp.set_pixel_size( pixel_size );

	background_color = black;
	tracer_ptr = new RayCast( this );

	if( camera_ptr ) {
		delete camera_ptr;
		camera_ptr = nullptr;
	}

	switch( camera ) {
	case Cameras::PIN : {
		Pinhole* pinhole_ptr = new Pinhole( );

		pinhole_ptr->set_eye( eye );
		pinhole_ptr->set_lookat( lookat );
		pinhole_ptr->set_up_vector( up );
		pinhole_ptr->set_roll( ra );
		pinhole_ptr->set_view_distance( d );
		pinhole_ptr->set_zoom( z );
		pinhole_ptr->set_exposure_time( e );

		camera_ptr = pinhole_ptr;
		pinhole_ptr = nullptr;
		break;
	}
	case Cameras::THIN : {
		ThinLense* thinlense_ptr = new ThinLense( );

		thinlense_ptr->set_eye( eye );
		thinlense_ptr->set_lookat( lookat );
		thinlense_ptr->set_up_vector( up );
		thinlense_ptr->set_roll( ra );
		thinlense_ptr->set_view_distance( d );
		thinlense_ptr->set_zoom( z );
		thinlense_ptr->set_exposure_time( e );
		thinlense_ptr->set_focal_distance( fd );
		thinlense_ptr->set_lens_radius( lr );

		thinlense_ptr->set_sampler( new Regular( 25 ) );
		camera_ptr = thinlense_ptr;
		thinlense_ptr = nullptr;
		break;
	}
	case Cameras::ORTHO : {
		Orthographic* default_ptr = new Orthographic( );

		default_ptr->set_eye( eye );
		default_ptr->set_lookat( lookat );
		default_ptr->set_up_vector( up );
		default_ptr->set_roll( ra );
		default_ptr->set_exposure_time( e );

		camera_ptr = default_ptr;
		default_ptr = nullptr;
		break;
	}
	default : {
		Orthographic* default_ptr = new Orthographic( );

		default_ptr->set_eye( eye );
		default_ptr->set_lookat( lookat );
		default_ptr->set_up_vector( up );
		default_ptr->set_roll( ra );
		default_ptr->set_exposure_time( e );

		camera_ptr = default_ptr;
		default_ptr = nullptr;
		break;
	}
	}

	camera_ptr->compute_uvw( );
}


void World::build( const std::vector<GeometricObject*>& objects, const std::vector<Light*>& lights ) {
	World::objects = objects;
	World::lights = lights;

	build_grid( );
}


void World::delete_objects( ) {
	const int num_objects = objects.size( );

	for( int j = 0 ; j < num_objects ; j++ ) {
		delete objects[j];
		objects[j] = nullptr;
	}

	objects.clear( );
}


void World::delete_lights( ) {
	const int num_lights = lights.size( );

	for( int j = 0 ; j < num_lights ; j++ ) {
		delete lights[j];
		lights[j] = nullptr;
	}

	lights.clear( );
}
