#include "ShadeRec.h"

#include "Constants.h"


ShadeRec::ShadeRec( World& wr )
	: hit_an_object( false ),
	  material_ptr( nullptr ),
	  depth( 0 ),
	  color( black ),
	  t( 0.0 ),
	  u( 0.0 ),
	  v( 0.0 ),
	  w( wr ) {}


ShadeRec::ShadeRec( const ShadeRec& sr ) = default;


ShadeRec::~ShadeRec( ) = default;
