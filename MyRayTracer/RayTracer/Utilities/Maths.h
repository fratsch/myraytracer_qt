#pragma once

#include <cstdlib>

#include "Constants.h"


double min( double d0, double d1 );

double max( double d0, double d1 );

int rand_int( );

float rand_float( );

void set_rand_see( int seed );

float rand_float( int l, float h );

int rand_int( int l, int h );

double clamp( double d, double min, double max );

int solve_quadric( const double c[3], double s[2] );

int solve_cubic( const double c[4], double s[3] );

int solve_quartic( double c[5], double s[4] );


inline double min( const double d0, const double d1 ) {
	return d0 < d1 ? d0 : d1;
}


inline double max( const double d0, const double d1 ) {
	return d0 > d1 ? d0 : d1;
}


inline int rand_int( ) {
	return rand( );
}


inline float rand_float( ) {
	return static_cast<float>( rand_int( ) ) * invRAND_MAX;
}


inline void set_rand_see( const int seed ) {
	srand( seed );
}


inline float rand_float( const int l, const float h ) {
	return rand_float( ) * ( h - l ) + l;
}


inline int rand_int( const int l, const int h ) {
	return static_cast<int>( rand_float( 0, h - l + 1 ) + l );
}


inline double clamp( const double d, const double min, const double max ) {
	return d < min ? min : ( d > max ? max : d );
}
