#pragma once

class Ray;
class Point3D;


class BoundingBox {

public:

	double x0{ }, y0{ }, z0{ }, x1{ }, y1{ }, z1{ };

	BoundingBox( );
	BoundingBox( double x0, double y0, double z0, double x1, double y1, double z1 );
	BoundingBox( const Point3D& p0, const Point3D& p1 );
	BoundingBox( const BoundingBox& bbox );
	~BoundingBox( );

	BoundingBox& operator=( const BoundingBox& rhs );

	bool hit( const Ray& ray ) const;

	bool inside( const Point3D& p ) const;
	
};
