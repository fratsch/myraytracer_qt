#pragma once

#include "Point3D.h"
#include "Normal.h"
#include "RGBColor.h"
#include "Ray.h"

class World;
class Material;


class ShadeRec {

public:

	explicit ShadeRec( World& wr );
	ShadeRec( const ShadeRec& sr );
	~ShadeRec( );

	// @formatter:off
	
	bool			hit_an_object;
	Material*		material_ptr;
	Point3D 		hit_point;
	Point3D			local_hit_point;
	Normal			normal;
	Ray				ray;
	int				depth;
	Vector3D		dir;
	RGBColor		color;
	double			t;
	float			u;
	float			v;
	World&			w;
	
	// @formatter:on
};
