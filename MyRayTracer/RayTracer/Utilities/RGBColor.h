#pragma once


class RGBColor {

public:

	float r{ }, g{ }, b{ };

	RGBColor( );
	explicit RGBColor( float c );
	RGBColor( float r, float g, float b );
	RGBColor( const RGBColor& c );
	~RGBColor( );

	RGBColor& operator=( const RGBColor& rhs );

	RGBColor operator+( const RGBColor& c ) const;

	RGBColor& operator+=( const RGBColor& c );

	RGBColor operator*( float a ) const;

	RGBColor& operator*=( float a );

	RGBColor operator/( float a ) const;

	RGBColor& operator/=( float a );

	RGBColor operator*( const RGBColor& c ) const;

	bool operator==( const RGBColor& c ) const;

	RGBColor powc( float p ) const;

	float average( ) const;
};


// inlined member functions

inline RGBColor RGBColor::operator+( const RGBColor& c ) const {
	return RGBColor( r + c.r, g + c.g, b + c.b );
}


inline RGBColor& RGBColor::operator+=( const RGBColor& c ) {
	r += c.r;
	g += c.g;
	b += c.b;
	return *this;
}


inline RGBColor RGBColor::operator*( const float a ) const {
	return RGBColor( r * a, g * a, b * a );
}


inline RGBColor& RGBColor::operator*=( const float a ) {
	r *= a;
	g *= a;
	b *= a;
	return *this;
}


inline RGBColor RGBColor::operator/( const float a ) const {
	return RGBColor( r / a, g / a, b / a );
}


inline RGBColor& RGBColor::operator/=( const float a ) {
	r /= a;
	g /= a;
	b /= a;
	return *this;
}


inline RGBColor RGBColor::operator*( const RGBColor& c ) const {
	return RGBColor( r * c.r, g * c.g, b * c.b );
}


inline bool RGBColor::operator==( const RGBColor& c ) const {
	return r == c.r && g == c.g && b == c.b;
}


inline float RGBColor::average( void ) const {
	return 0.333333333333f * ( r + g + b );
}


// inlined non-member function

RGBColor operator*( float a, const RGBColor& c );

inline RGBColor operator*( const float a, const RGBColor& c ) {
	return RGBColor( a * c.r, a * c.g, a * c.b );
}
