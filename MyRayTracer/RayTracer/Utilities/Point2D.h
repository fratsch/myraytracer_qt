#pragma once


class Point2D {
public:

	double x{ }, y{ };

	Point2D( );
	explicit Point2D( float arg );
	Point2D( float x1, float y1 );
	Point2D( const Point2D& p );
	~Point2D( );

	Point2D& operator=( const Point2D& rhs );

	Point2D operator*( float a ) const;
	
};


// scales the point by the float a

inline Point2D Point2D::operator*( const float a ) const {
	return ( Point2D( a * x, a * y ) );
}
