#pragma once

#include "Matrix.h"
#include "Vector3D.h"


class Normal {

public:

	double x{ }, y{ }, z{ };

	Normal( );
	explicit Normal( double a );
	Normal( double x, double y, double z );
	Normal( const Normal& n );
	explicit Normal( const Vector3D& v );
	~Normal( );

	Normal& operator=( const Normal& rhs );

	Normal& operator=( const Vector3D& rhs );

	Normal& operator=( const Point3D& rhs );

	Normal operator-( ) const;

	Normal operator+( const Normal& n ) const;

	Normal& operator+=( const Normal& n );

	double operator*( const Vector3D& v ) const;

	Normal operator*( double a ) const;

	void normalize( );
	
};


inline Normal Normal::operator-( ) const {
	return Normal( -x, -y, -z );
}


inline Normal Normal::operator+( const Normal& n ) const {
	return Normal( x + n.x, y + n.y, z + n.z );
}


inline Normal& Normal::operator+=( const Normal& n ) {
	x += n.x;
	y += n.y;
	z += n.z;
	return ( *this );
}


inline double Normal::operator*( const Vector3D& v ) const {
	return x * v.x + y * v.y + z * v.z;
}


inline Normal Normal::operator*( const double a ) const {
	return Normal( x * a, y * a, z * a );
}


// inlined non-member functions

Normal operator*( double f, const Normal& n );

inline Normal operator*( const double f, const Normal& n ) {
	return Normal( f * n.x, f * n.y, f * n.z );
}


Vector3D operator+( const Vector3D& v, const Normal& n );

inline Vector3D operator+( const Vector3D& v, const Normal& n ) {
	return Vector3D( v.x + n.x, v.y + n.y, v.z + n.z );
}


Vector3D operator-( const Vector3D&, const Normal& n );

inline Vector3D operator-( const Vector3D& v, const Normal& n ) {
	return Vector3D( v.x - n.x, v.y - n.y, v.z - n.z );
}


double operator*( const Vector3D& v, const Normal& n );

inline double operator*( const Vector3D& v, const Normal& n ) {
	return v.x * n.x + v.y * n.y + v.z * n.z;
}

// non-inlined non-member function

Normal operator*( const Matrix& mat, const Normal& n );
