#include "Normal.h"

#include <cmath>

#include "Point3D.h"


Normal::Normal( )
	: x( 0.0 ),
	  y( 0.0 ),
	  z( 0.0 ) {}


Normal::Normal( const double a )
	: x( a ),
	  y( a ),
	  z( a ) {}


Normal::Normal( const double x, const double y, const double z )
	: x( x ),
	  y( y ),
	  z( z ) {}


Normal::Normal( const Normal& n ) = default;


Normal::Normal( const Vector3D& v )
	: x( v.x ),
	  y( v.y ),
	  z( v.z ) {}


Normal::~Normal( ) = default;


Normal& Normal::operator=( const Normal& rhs ) {
	if( this == &rhs )
		return *this;

	x = rhs.x;
	y = rhs.y;
	z = rhs.z;

	return *this;
}


Normal& Normal::operator=( const Vector3D& rhs ) {
	x = rhs.x;
	y = rhs.y;
	z = rhs.z;
	return *this;
}


Normal& Normal::operator=( const Point3D& rhs ) {
	x = rhs.x;
	y = rhs.y;
	z = rhs.z;
	return *this;
}

void Normal::normalize( void ) {
	const double length = sqrt( x * x + y * y + z * z );
	x /= length;
	y /= length;
	z /= length;
}


// non-member function definition

// ---------------------------------------------------------- operator*
// multiplication by a matrix on the left

Normal operator*( const Matrix& mat, const Normal& n ) {
	return Normal( mat.m[0][0] * n.x + mat.m[1][0] * n.y + mat.m[2][0] * n.z,
	               mat.m[0][1] * n.x + mat.m[1][1] * n.y + mat.m[2][1] * n.z,
	               mat.m[0][2] * n.x + mat.m[1][2] * n.y + mat.m[2][2] * n.z );
}
