#pragma once

#include "Point3D.h"
#include "Vector3D.h"


class Ray {

public:

	Point3D o;
	Vector3D d;

	Ray( );
	Ray( const Point3D& origin, const Vector3D& dir );
	Ray( const Ray& ray );
	~Ray( );

	Ray& operator=( const Ray& rhs );

};
