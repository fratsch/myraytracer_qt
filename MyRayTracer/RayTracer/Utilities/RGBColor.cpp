#include "RGBColor.h"

#include <cmath>


RGBColor::RGBColor( )
	: r( 0.0 ),
	  g( 0.0 ),
	  b( 0.0 ) {}


RGBColor::RGBColor( const float c )
	: r( c ),
	  g( c ),
	  b( c ) {}


RGBColor::RGBColor( const float r, const float g, const float b )
	: r( r ),
	  g( g ),
	  b( b ) {}


RGBColor::RGBColor( const RGBColor& c ) = default;


RGBColor::~RGBColor( ) = default;


RGBColor& RGBColor::operator=( const RGBColor& rhs ) {
	if( this == &rhs )
		return *this;

	r = rhs.r;
	g = rhs.g;
	b = rhs.b;

	return *this;
}


RGBColor RGBColor::powc( float p ) const {
	return RGBColor( pow( r, p ), pow( g, p ), pow( b, p ) );
}
