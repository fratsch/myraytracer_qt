#include "BoundingBox.h"

#include "Point3D.h"
#include "Ray.h"
#include "Constants.h"


BoundingBox::BoundingBox( )
	: x0( -1.0 ),
	  y0( -1.0 ),
	  z0( -1.0 ),
	  x1( 1.0 ),
	  y1( 1.0 ),
	  z1( 1.0 ) {}


BoundingBox::BoundingBox( const double x0, const double y0, const double z0, const double x1, const double y1, const double z1 )
	: x0( x0 ),
	  y0( y0 ),
	  z0( z0 ),
	  x1( x1 ),
	  y1( y1 ),
	  z1( z1 ) { }


BoundingBox::BoundingBox( const Point3D& p0, const Point3D& p1 )
	: x0( p0.x ),
	  y0( p0.y ),
	  z0( p0.z ),
	  x1( p1.x ),
	  y1( p1.y ),
	  z1( p1.z ) { }


BoundingBox::BoundingBox( const BoundingBox& bbox ) = default;


BoundingBox::~BoundingBox( ) = default;


BoundingBox& BoundingBox::operator=( const BoundingBox& rhs ) {

	if( this == &rhs )
		return *this;

	x0 = rhs.x0;
	y0 = rhs.y0;
	z0 = rhs.z0;
	x1 = rhs.x1;
	y1 = rhs.y1;
	z1 = rhs.z1;

	return *this;
}


bool BoundingBox::hit( const Ray& ray ) const {

	const double ox = ray.o.x;
	const double oy = ray.o.y;
	const double oz = ray.o.z;
	const double dx = ray.d.x;
	const double dy = ray.d.y;
	const double dz = ray.d.z;

	double tx_min, ty_min, tz_min;
	double tx_max, ty_max, tz_max;

	const double a = 1.0 / dx;
	if( a >= 0.0 ) {
		tx_min = ( x0 - ox ) * a;
		tx_max = ( x1 - ox ) * a;
	}
	else {
		tx_min = ( x1 - ox ) * a;
		tx_max = ( x0 - ox ) * a;
	}

	double b = 1.0 / dy;
	if( b >= 0.0 ) {
		ty_min = ( y0 - oy ) * b;
		ty_max = ( y1 - oy ) * b;
	}
	else {
		ty_min = ( y1 - oy ) * b;
		ty_max = ( y0 - oy ) * b;
	}

	double c = 1.0 / dz;
	if( c >= 0.0 ) {
		tz_min = ( z0 - oz ) * c;
		tz_max = ( z1 - oz ) * c;
	}
	else {
		tz_min = ( z1 - oz ) * c;
		tz_max = ( z0 - oz ) * c;
	}

	double t0, t1;

	if( tx_min > ty_min )
		t0 = tx_min;
	else
		t0 = ty_min;
	if( tz_min > t0 )
		t0 = tz_min;

	if( tx_max < ty_max )
		t1 = tx_max;
	else
		t1 = ty_max;
	if( tz_max < t1 )
		t1 = tz_max;

	return t0 < t1 && t1 > kEpsilon;
}


bool BoundingBox::inside( const Point3D& p ) const {
	return ( p.x > x0 && p.x < x1 ) && ( p.y > y0 && p.y < y1 ) && ( p.z > z0 && p.z < z1 );
}
