#include "GlossySpecular.h"

#include <cmath>

#include "Sampler.h"
#include "ShadeRec.h"
#include "Constants.h"


GlossySpecular::GlossySpecular( )
	: BRDF( ),
	  ks( 0.0 ),
	  cs( 1.0 ),
	  exp( 1.0 ) {}


GlossySpecular::GlossySpecular( const GlossySpecular& glossy ) = default;


GlossySpecular::GlossySpecular( const float ks, const RGBColor& cs, const float exp, Sampler* sampler )
	: BRDF( ),
	  ks( ks ),
	  cs( cs ),
	  exp( exp ) {}


GlossySpecular* GlossySpecular::clone( ) const {
	return new GlossySpecular( *this );
}


GlossySpecular::~GlossySpecular( ) = default;


RGBColor GlossySpecular::f( const ShadeRec& sr, const Vector3D& wo, const Vector3D& wi ) const {

	RGBColor L;
	const float ndotwi = static_cast<float>( sr.normal * wi );
	const Vector3D r = -wi + 2.0 * sr.normal * ndotwi;
	const float rdotwo = static_cast<float>( r * wo );

	if( rdotwo > 0.0f )
		L = RGBColor( static_cast<float>( ks * pow( rdotwo, exp ) ) );

	return L;
}


RGBColor GlossySpecular::rho( const ShadeRec& sr, const Vector3D& wo ) const {
	return black;
}
