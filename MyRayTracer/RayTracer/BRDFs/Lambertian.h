#pragma once

#include "BRDF.h"

class Lambertian : public BRDF {

public:

	Lambertian( );
	Lambertian( const Lambertian& lamb );
	Lambertian( float kd, const RGBColor& color );
	Lambertian* clone( ) const override;
	virtual ~Lambertian( );
	Lambertian& operator=( const Lambertian& rhs );

	RGBColor f( const ShadeRec& sr, const Vector3D& wo, const Vector3D& wi ) const override;

	RGBColor rho( const ShadeRec& sr, const Vector3D& wo ) const override;

	void set_ka( float ka );

	void set_kd( float kd );

	void set_cd( const RGBColor& cd );

	void set_cd( float r, float g, float b );

	void set_cd( float c );

private:
	// @formatter:off
	
	float		kd{ };
	RGBColor	cd;

	// @formatter:on
};


inline void Lambertian::set_ka( const float ka ) {
	kd = ka;
}


inline void Lambertian::set_kd( const float kd ) {
	Lambertian::kd = kd;
}


inline void Lambertian::set_cd( const RGBColor& cd ) {
	Lambertian::cd = cd;
}


inline void Lambertian::set_cd( const float r, const float g, const float b ) {
	cd.r = r;
	cd.g = g;
	cd.b = b;
}


inline void Lambertian::set_cd( const float c ) {
	cd.r = c;
	cd.g = c;
	cd.b = c;
}
