#include "PerfectSpecular.h"

#include "Constants.h"
#include "ShadeRec.h"


// ---------------------------------------------------------- default constructor
PerfectSpecular::PerfectSpecular( )
	: BRDF( ),
	  kr( 0.0 ),
	  cr( 1.0 ) {}


// ---------------------------------------------------------------------- clone
PerfectSpecular*
PerfectSpecular::clone( ) const {
	return new PerfectSpecular( *this );
}


// ---------------------------------------------------------- destructor
PerfectSpecular::~PerfectSpecular( ) = default;


// ---------------------------------------------------------- f
RGBColor PerfectSpecular::f( const ShadeRec& sr, const Vector3D& wo, const Vector3D& wi ) const {
	return black;
}


// ---------------------------------------------------------- sample_f
RGBColor PerfectSpecular::sample_f( const ShadeRec& sr, const Vector3D& wo, Vector3D& wi ) const {
	float ndotwo = sr.normal * wo;
	wi = -wo + 2.0 * sr.normal * ndotwo;

	return kr * cr / fabs( sr.normal * wi );
}


// ---------------------------------------------------------- sample_f
RGBColor PerfectSpecular::sample_f( const ShadeRec& sr, const Vector3D& wo, Vector3D& wi, float& pdf ) const {
	float ndotwo = sr.normal * wo;
	wi = -wo + 2.0 * sr.normal * ndotwo;
	pdf = fabs( sr.normal * wi );

	return kr * cr;
}


// ---------------------------------------------------------- rho
RGBColor PerfectSpecular::rho( const ShadeRec& sr, const Vector3D& wo ) const {
	return black;
}
