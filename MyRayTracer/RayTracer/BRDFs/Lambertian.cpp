#include "Lambertian.h"

#include "Constants.h"
#include "ShadeRec.h"


Lambertian::Lambertian( )
	: BRDF( ),
	  kd( 0.0 ),
	  cd( 0.0 ) {}


Lambertian::Lambertian( const Lambertian& lamb ) = default;


Lambertian::Lambertian( const float kd, const RGBColor& color )
	: kd( kd ),
	  cd( color ) {}


Lambertian* Lambertian::clone( ) const {
	return new Lambertian( *this );
}


Lambertian::~Lambertian( ) = default;


Lambertian& Lambertian::operator=( const Lambertian& rhs ) {
	if( this == &rhs )
		return *this;

	BRDF::operator=( rhs );

	kd = rhs.kd;
	cd = rhs.cd;

	return *this;
}


RGBColor Lambertian::f( const ShadeRec& sr, const Vector3D& wo, const Vector3D& wi ) const {
	return kd * cd * static_cast<float>( invPI );
}


RGBColor Lambertian::rho( const ShadeRec& sr, const Vector3D& wo ) const {
	return kd * cd;
}
