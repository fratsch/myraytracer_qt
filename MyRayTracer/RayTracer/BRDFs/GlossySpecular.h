#pragma once

#include "BRDF.h"

class Normal;


class GlossySpecular : public BRDF {

public:

	GlossySpecular( );
	GlossySpecular( const GlossySpecular& glossy );
	GlossySpecular( float ks, const RGBColor& cs, float exp, Sampler* sampler );
	GlossySpecular* clone( ) const override;
	~GlossySpecular( );

	RGBColor f( const ShadeRec& sr, const Vector3D& wo, const Vector3D& wi ) const override;

	RGBColor rho( const ShadeRec& sr, const Vector3D& wo ) const override;

	void set_ks( float ks );

	void set_exp( float exp );

	void set_cs( const RGBColor& cs );

	void set_cs( float r, float g, float b );

	void set_cs( float c );

private:
	// @formatter:off

	float		ks{ };
	RGBColor	cs;
	float		exp{ };

	// @formatter:on
};


inline void GlossySpecular::set_ks( const float ks ) {
	GlossySpecular::ks = ks;
}


inline void GlossySpecular::set_exp( const float exp ) {
	GlossySpecular::exp = exp;
}


inline void GlossySpecular::set_cs( const RGBColor& cs ) {
	GlossySpecular::cs = cs;
}


inline void GlossySpecular::set_cs( const float r, const float g, const float b ) {
	cs.r = r;
	cs.g = g;
	cs.b = b;
}


inline void GlossySpecular::set_cs( const float c ) {
	cs.r = c;
	cs.g = c;
	cs.b = c;
}
