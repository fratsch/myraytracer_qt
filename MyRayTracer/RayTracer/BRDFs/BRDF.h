#pragma once

#include "RGBColor.h"

class Sampler;
class ShadeRec;
class Vector3D;


class BRDF {

public:
	BRDF( ) = default;
	BRDF( const BRDF& brdf ) = default;
	virtual ~BRDF( ) = default;

	BRDF& operator=( const BRDF& rhs ) = default;

	virtual BRDF* clone( ) const = 0;

	virtual RGBColor f( const ShadeRec& sr, const Vector3D& wo, const Vector3D& wi ) const = 0;

	virtual RGBColor rho( const ShadeRec& sr, const Vector3D& wo ) const = 0;

};
