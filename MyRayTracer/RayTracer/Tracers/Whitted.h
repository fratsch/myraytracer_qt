#pragma once

#include "Tracer.h"


class Whitted : public Tracer {

public:

	Whitted( );
	Whitted( World* world_ptr );
	virtual ~Whitted( );

	virtual RGBColor trace_ray( Ray ray, int depth );

};
