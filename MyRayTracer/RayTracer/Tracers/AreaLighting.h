#pragma once

#include "Tracer.h"


class AreaLighting: public Tracer {

public:

	AreaLighting();
	AreaLighting(World* world_ptr);
	virtual ~AreaLighting();

	virtual RGBColor trace_ray( Ray ray, int depth) const;
};