#pragma once

#include "Ray.h"
#include "RGBColor.h"

class World;


class Tracer {
	
public:

	Tracer( );
	explicit Tracer( World* world_ptr );
	virtual ~Tracer( );

	virtual RGBColor trace_ray( const Ray& ray, int depth ) const = 0;

	virtual RGBColor trace_ray( const Ray& ray, float& tmin, int depth ) const = 0;

protected:

	World* world_ptr;

};
