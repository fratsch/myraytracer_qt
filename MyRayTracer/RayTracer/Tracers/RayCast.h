#pragma once

#include "Tracer.h"

class RayCast final : public Tracer {

public:

	RayCast( );
	explicit RayCast( World* _worldPtr );
	virtual ~RayCast( );

	RGBColor trace_ray( const Ray& ray, int depth ) const override;

	RGBColor trace_ray( const Ray& ray, float& tmin, int depth ) const override;

};
