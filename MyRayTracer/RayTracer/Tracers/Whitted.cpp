#include "Whitted.h"

#include "World.h"
#include "ShadeRec.h"
#include "Constants.h"
#include "Material.h"


Whitted::Whitted( )
	: Tracer( ) { }


Whitted::Whitted( World* world_ptr )
	: Tracer( world_ptr ) { }


Whitted::~Whitted( ) = default;

RGBColor Whitted::trace_ray( const Ray ray, const int depth ) {
	if (depth > world_ptr->vp.max_depth)
		return black;
	else {
		ShadeRec sr(world_ptr->hit_objects(ray));

		if (sr.hit_an_object) {
			sr.depth = depth;
			sr.ray = ray;
			return sr.material_ptr->shade(sr);
		}
		else
			return world_ptr->background_color;
	}
}

