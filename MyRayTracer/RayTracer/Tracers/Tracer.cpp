#include "Tracer.h"

#include "Constants.h"
#include "ShadeRec.h"
#include "World.h"
#include "Material.h"


Tracer::Tracer( )
	: world_ptr( nullptr ) {}


Tracer::Tracer( World* world_ptr )
	: world_ptr( world_ptr ) {}


Tracer::~Tracer( ) {
	if( world_ptr )
		world_ptr = nullptr;
}


RGBColor Tracer::trace_ray( const Ray& ray, const int depth ) const {
	ShadeRec sr( world_ptr->hit_objects( ray ) );

	if( sr.hit_an_object ) {
		sr.ray = ray;
		return ( sr.material_ptr->shade( sr ) );
	}
	else
		return world_ptr->background_color;
}


RGBColor Tracer::trace_ray( const Ray& ray, float& tmin, const int depth ) const {
	return black;
}
