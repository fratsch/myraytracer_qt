#pragma once

#include "GeometricObject.h"

#include "Point3D.h"
#include "Normal.h"


class Plane : public GeometricObject {

public:

	Plane( );
	Plane( const Point3D& point, const Normal& normal );
	Plane( const Plane& plane );
	Plane* clone( ) const override;
	~Plane( ) override;

	Plane& operator=( const Plane& rhs );

	bool hit( const Ray& ray, double& tmin, ShadeRec& sr ) const override;

	bool shadow_hit( const Ray& ray, double& tmin ) const override;

private:

	Point3D a;
	Normal n;

};
