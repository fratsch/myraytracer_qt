#include "Rectangle.h"

#include "Ray.h"
#include "ShadeRec.h"
#include "Constants.h"


Rectangle::Rectangle( )
	: GeometricObject( ),
	  p( -1.0, 0.0, -1.0 ),
	  a( 0.0, 0.0, 1.0 ),
	  b( 1.0, 0.0, 0.0 ),
	  a_len_squared( 1.0 ),
	  b_len_squared( 1.0 ) { }


Rectangle::Rectangle( const Point3D& p, const Vector3D& a, const Vector3D b )
	: GeometricObject( ),
	  p( p ),
	  a( a ),
	  b( b ),
	  a_len_squared( a.len_squared( ) ),
	  b_len_squared( b.len_squared( ) ) {
	normal = a ^ b;
	normal.normalize( );
}


Rectangle::Rectangle( const Point3D& p, const Vector3D& a, const Vector3D b, Normal& n )
	: GeometricObject( ),
	  p( p ),
	  a( a ),
	  b( b ),
	  a_len_squared( a.len_squared( ) ),
	  b_len_squared( b.len_squared( ) ),
	  normal( n ) {
	normal.normalize( );
}


Rectangle::Rectangle( const Rectangle& rect )
	: GeometricObject( rect ),
	  p( rect.p ),
	  a( rect.a ),
	  b( rect.b ),
	  a_len_squared( rect.a.len_squared( ) ),
	  b_len_squared( rect.b.len_squared( ) ),
	  normal( rect.normal ) { }


Rectangle* Rectangle::clone( ) const {
	return new Rectangle( *this );
}


Rectangle::~Rectangle( ) = default;


Rectangle& Rectangle::operator=( const Rectangle& rhs ) {
	if( this == &rhs )
		return *this;

	GeometricObject::operator=( rhs );

	p = rhs.p;
	a = rhs.a;
	b = rhs.b;
	a_len_squared = rhs.a_len_squared;
	b_len_squared = rhs.b_len_squared;
	normal = rhs.normal;

	return *this;
}


bool Rectangle::hit( const Ray& ray, double& tmin, ShadeRec& sr ) const {

	double t = ( p - ray.o ) * normal / ( ray.d * normal );

	if( t < kEpsilon )
		return false;

	const Point3D p = ray.o + t * ray.d;
	const Vector3D d = p - Rectangle::p;

	const double ddota = d * a;

	if( ddota < 0.0 || ddota > a_len_squared )
		return false;

	const double ddotb = d * b;

	if( ddotb < 0.0 || ddotb > b_len_squared )
		return false;

	tmin = t;
	sr.normal = normal;
	if( ray.d * normal < 0.0 )
		sr.normal = -normal;
	sr.local_hit_point = p;

	return true;
}

bool Rectangle::shadow_hit( const Ray& ray, double& tmin ) const {
	const double t = ( p - ray.o ) * normal / ( ray.d * normal );

	if( t < kEpsilon )
		return false;

	const Point3D p = ray.o + t * ray.d;
	const Vector3D d = p - Rectangle::p;

	const double ddota = d * a;

	if( ddota < 0.0 || ddota > a_len_squared )
		return false;

	const double ddotb = d * b;

	if( ddotb < 0.0 || ddotb > b_len_squared )
		return false;

	return true;
}
