#include "Compound.h"

#include "Constants.h"
#include "Normal.h"
#include "ShadeRec.h"


Compound::Compound( )
	: GeometricObject( ) { }


Compound::Compound( const Compound& comp )
	: GeometricObject( comp ) {
	copy_objects( comp.objects );
}


Compound* Compound::clone( ) const {
	return new Compound( *this );
}


Compound::~Compound( ) {
	delete_objects( );
}


Compound& Compound::operator=( const Compound& rhs ) {
	if( this == &rhs )
		return *this;

	GeometricObject::operator=( rhs );

	copy_objects( rhs.objects );

	return *this;
}


void Compound::add_object( GeometricObject* object_ptr ) {
	objects.push_back( object_ptr );
}


void Compound::set_material( Material* material_ptr ) {
	const int num_objects = objects.size( );

	for( int j = 0 ; j < num_objects ; ++j )
		objects[j]->set_material( material_ptr );
}


void Compound::delete_objects( ) {
	const int num_objects = objects.size( );

	for( int i = 0 ; i < num_objects ; ++i ) {
		delete objects[i];
		objects[i] = nullptr;
	}

	objects.erase( objects.begin( ), objects.end( ) );
}


void Compound::copy_objects( const std::vector<GeometricObject*>& rhs_objects ) {
	delete_objects( );
	
	const int num_objects = rhs_objects.size( );

	for( int i = 0 ; i < num_objects ; ++i )
		objects.push_back( rhs_objects[i]->clone( ) );
}


bool Compound::hit( const Ray& ray, double& tmin, ShadeRec& sr ) const {
	double t;
	Normal normal;
	Point3D local_hit_point;
	bool hit = false;
	tmin = kHugeValue;

	const int num_objects = objects.size( );

	for( int i = 0 ; i < num_objects ; ++i ) {
		if( objects[i]->hit( ray, t, sr ) && ( t < tmin ) ) {
			hit = true;
			tmin = t;
			material_ptr = objects[i]->get_material( );
			normal = sr.normal;
			local_hit_point = sr.local_hit_point;
		}
	}

	if( hit ) {
		sr.t = tmin;
		sr.normal = normal;
		sr.local_hit_point = local_hit_point;
	}

	return hit;
}
