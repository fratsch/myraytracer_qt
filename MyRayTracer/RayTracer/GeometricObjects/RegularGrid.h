#pragma once

#include "Compound.h"


class RegularGrid : public Compound {

public:

	RegularGrid();
	RegularGrid(const RegularGrid& reg);
	RegularGrid* clone() const override;
	virtual ~RegularGrid();

	RegularGrid& operator=(const RegularGrid& rhs);

	virtual BoundingBox get_bounding_box();

	bool hit(const Ray& ray, double& tmin, ShadeRec& sr) const override;

	void setup_cells();

private:

	std::vector<GeometricObject*> cells;
	int nx, ny, nz;
	BoundingBox bbox;

	Point3D find_min_bounds();
	Point3D find_max_bounds();
	
};