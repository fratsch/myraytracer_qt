#include "BoundedObject.h"


BoundedObject::BoundedObject( )
	: GeometricObject( ),
	  bbox( BoundingBox( ) ) { }


BoundedObject::BoundedObject( const BoundingBox& bounding_box, const bool shadows )
	: GeometricObject( shadows ),
	  bbox( bounding_box ) { }


BoundedObject::BoundedObject( const BoundedObject& bounded_object )
	: GeometricObject( bounded_object.shadows ),
	  bbox( bounded_object.bbox ) { }


BoundedObject::~BoundedObject( ) = default;


BoundedObject& BoundedObject::operator=( const BoundedObject& rhs ) {
	if( this == &rhs )
		return *this;

	GeometricObject::operator=( rhs );

	bbox = rhs.bbox;

	return *this;
}


void BoundedObject::set_bounding_box( const BoundingBox& bounding_box ) {
	bbox = bounding_box;
}


BoundingBox BoundedObject::get_bounding_box( ) const {
	return bbox;
}
