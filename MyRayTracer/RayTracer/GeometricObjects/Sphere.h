#pragma once

#include "BoundedObject.h"

#include "Point3D.h"


class Sphere final : public BoundedObject {

public:

	Sphere( );
	Sphere( Point3D center, double r );
	Sphere( const Sphere& sphere );
	Sphere* clone( ) const override;
	virtual ~Sphere( );

	Sphere& operator=( const Sphere& rhs );

	void set_center( const Point3D& c );

	void set_center( double x, double y, double z );

	void set_radius( double r );

	bool hit( const Ray& ray, double& tmin, ShadeRec& sr ) const override;

	bool shadow_hit( const Ray& ray, double& tmin ) const override;

private:

	Point3D center;
	double radius;

	void create_bounding_box( );

};


inline void Sphere::set_center( const Point3D& c ) {
	center = c;
}

inline void Sphere::set_center( const double x, const double y, const double z ) {
	center.x = x;
	center.y = y;
	center.z = z;
}

inline void Sphere::set_radius( const double r ) {
	radius = r;
}
