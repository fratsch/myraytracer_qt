#include "Plane.h"

#include "Ray.h"
#include "ShadeRec.h"
#include "Constants.h"


Plane::Plane(  )
	: GeometricObject( ),
	  a( 0.0 ),
	  n( 0, 1, 0 ) {}


Plane::Plane( const Point3D& point, const Normal& normal )
	: GeometricObject( ),
	  a( point ),
	  n( normal ) {
	n.normalize( );
}


Plane::Plane( const Plane& plane ) = default;


Plane* Plane::clone( ) const {
	return new Plane( *this );
}


Plane& Plane::operator=( const Plane& rhs ) {

	if( this == &rhs )
		return *this;

	GeometricObject::operator=( rhs );

	a = rhs.a;
	n = rhs.n;

	return *this;
}


Plane::~Plane( ) = default;


bool Plane::hit( const Ray& ray, double& tmin, ShadeRec& sr ) const {
	const float t = ( a - ray.o ) * n / ( ray.d * n );

	if( t > kEpsilon ) {
		tmin = t;
		sr.normal = n;
		if( ray.d * n > 0.0f )
			sr.normal = -n;
		sr.local_hit_point = ray.o + t * ray.d;

		return true;
	}

	return false;
}

bool Plane::shadow_hit( const Ray& ray, double& tmin ) const {

	if( !shadows )
		return false;

	const float t = ( a - ray.o ) * n / ( ray.d * n );

	if( t > kEpsilon ) {
		tmin = t;
		return true;
	}

	return false;
}
