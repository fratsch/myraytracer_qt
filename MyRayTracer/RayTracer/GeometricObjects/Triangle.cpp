#include "Triangle.h"

#include "ShadeRec.h"
#include "Ray.h"
#include "Constants.h"


Triangle::Triangle( )
	: GeometricObject( ),
	  a( 0.0, 0.0, 0.0 ),
	  b( 0.0, 0.0, 1.0 ),
	  c( 1.0, 0.0, 0.0 ),
	  normal( 0.0, 1.0, 0.0 ) { }


Triangle::Triangle( const Point3D& a, const Point3D& b, const Point3D& c )
	: GeometricObject( ),
	  a( a ),
	  b( b ),
	  c( c ) {
	compute_normal( );
}


Triangle::Triangle(const Triangle& triangle) = default;


Triangle* Triangle::clone( ) const {
	return new Triangle( *this );
}


Triangle& Triangle::operator=( const Triangle& rhs ) {
	if( this == &rhs )
		return *this;

	GeometricObject::operator=( rhs );

	a = rhs.a;
	b = rhs.b;
	c = rhs.c;
	normal = rhs.normal;

	return *this;
}


Triangle::~Triangle( ) = default;


void Triangle::compute_normal( ) {
	normal =  b - a  ^  c - a;
	normal.normalize( );
}


bool Triangle::hit( const Ray& ray, double& tmin, ShadeRec& sr ) const {
	const double a = Triangle::a.x - b.x;
	const double b = Triangle::a.x - c.x;
	const double c = ray.d.x;
	const double d = Triangle::a.x - ray.o.x;
	const double e = Triangle::a.y - Triangle::b.y;
	const double f = Triangle::a.y - Triangle::c.y;
	const double g = ray.d.y;
	const double h = Triangle::a.y - ray.o.y;
	const double i = Triangle::a.z - Triangle::b.z;
	const double j = Triangle::a.z - Triangle::c.z;
	const double k = ray.d.z;
	const double l = Triangle::a.z - ray.o.z;
 
	const double m = f * k - g * j;
	const double n = h * k - g * l;
	const double p = f * l - h * j;
	const double q = g * i - e * k;
	const double s = e * j - f * i;

	const double inv_denom = 1.0 / ( a * m + b * q + c * s );

	const double e1 = d * m - b * n - c * p;
	const double beta = e1 * inv_denom;

	if( beta < 0.0 )
		return false;

	const double r = e * l - h * i;
	const double e2 = a * n + d * q + c * r;
	const double gamma = e2 * inv_denom;

	if( gamma < 0.0 )
		return false;

	if( beta + gamma > 1.0 )
		return false;

	const double e3 = a * p - b * r + d * s;
	const double t = e3 * inv_denom;

	if( t < kEpsilon )
		return false;

	tmin = t;
	sr.normal = normal;
	if( ray.d * normal > 0.0f )
		sr.normal = -normal;
	sr.local_hit_point = ray.o + t * ray.d;

	return true;
}


bool Triangle::shadow_hit( const Ray& ray, double& tmin ) const {
	const double a = Triangle::a.x - b.x;
	const double b = Triangle::a.x - c.x;
	const double c = ray.d.x;
	const double d = Triangle::a.x - ray.o.x;
	const double e = Triangle::a.y - Triangle::b.y;
	const double f = Triangle::a.y - Triangle::c.y;
	const double g = ray.d.y;
	const double h = Triangle::a.y - ray.o.y;
	const double i = Triangle::a.z - Triangle::b.z;
	const double j = Triangle::a.z - Triangle::c.z;
	const double k = ray.d.z;
	const double l = Triangle::a.z - ray.o.z;
 
	const double m = f * k - g * j;
	const double n = h * k - g * l;
	const double p = f * l - h * j;
	const double q = g * i - e * k;
	const double s = e * j - f * i;

	const double inv_denom = 1.0 / ( a * m + b * q + c * s );

	const double e1 = d * m - b * n - c * p;
	const double beta = e1 * inv_denom;

	if( beta < 0.0 )
		return false;

	const double r = e * l - h * i;
	const double e2 = a * n + d * q + c * r;
	const double gamma = e2 * inv_denom;

	if( gamma < 0.0 )
		return false;

	if( beta + gamma > 1.0 )
		return false;

	const double e3 = a * p - b * r + d * s;
	const double t = e3 * inv_denom;

	if( t < kEpsilon )
		return false;

	return true;
}
