#pragma once

#include "GeometricObject.h"
#include "Point3D.h"
#include "Normal.h"


class Disk final : public GeometricObject {

public:

	Disk( );
	Disk( const Point3D& p, const Normal& normal, double radius );
	Disk( const Disk& disk );
	Disk* clone( ) const override;
	virtual ~Disk( );

	Disk& operator=( const Disk& rhs );

	bool hit( const Ray& ray, double& tmin, ShadeRec& sr ) const override;

	bool shadow_hit( const Ray& ray, double& tmin ) const override;

private:
	// @formatter:off
	
	Point3D		p;
	Normal		normal;
	double		r{};
	double		r_squared{};

	// @formatter:on
};
