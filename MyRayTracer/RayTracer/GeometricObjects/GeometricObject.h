#pragma once

#include "BoundingBox.h"

class ShadeRec;
class Ray;
class Material;


class GeometricObject {

public:

	GeometricObject( );
	explicit GeometricObject( bool shadows );
	GeometricObject( const GeometricObject& object );
	virtual GeometricObject* clone( ) const = 0;
	virtual ~GeometricObject( );

	virtual bool hit( const Ray& ray, double& tmin, ShadeRec& sr ) const = 0;

	Material* get_material( ) const;

	virtual void set_material( Material* material_ptr );

	virtual bool shadow_hit( const Ray& ray, double& tmin ) const;

protected:

	mutable Material* material_ptr;

	bool shadows;

	GeometricObject& operator=( const GeometricObject& rhs );

};


inline Material* GeometricObject::get_material( ) const {
	return material_ptr;
}
