#include "GeometricObject.h"

#include "Material.h"


GeometricObject::GeometricObject( )
	: material_ptr( nullptr ),
	  shadows( true ) {}


GeometricObject::GeometricObject( const bool shadows )
	: material_ptr( nullptr ),
	  shadows( shadows ) { }


GeometricObject::GeometricObject( const GeometricObject& object ) {
	if( object.material_ptr )
		material_ptr = object.material_ptr;
	else
		material_ptr = nullptr;

	shadows = object.shadows;
}


GeometricObject& GeometricObject::operator=( const GeometricObject& rhs ) {

	if( this == &rhs )
		return *this;

	if( material_ptr ) {
		material_ptr = nullptr;
	}

	if( rhs.material_ptr )
		material_ptr = rhs.material_ptr;

	shadows = rhs.shadows;

	return *this;
}


GeometricObject::~GeometricObject( ) {
	if( material_ptr ) {
		delete material_ptr;
		material_ptr = nullptr;
	}
}


void GeometricObject::set_material( Material* material_ptr ) {
	GeometricObject::material_ptr = material_ptr;
}


bool GeometricObject::shadow_hit( const Ray& ray, double& tmin ) const {
	return false;
}
