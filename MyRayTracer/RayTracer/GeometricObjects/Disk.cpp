#include "Disk.h"

#include "Ray.h"
#include "ShadeRec.h"
#include "Constants.h"


Disk::Disk( )
	: GeometricObject( ),
	  p( 0.0, 0.0, 0.0 ),
	  normal( 0.0, 1.0, 0.0 ),
	  r( 1.0 ),
	  r_squared( 1.0 ) {}


Disk::Disk( const Point3D& p, const Normal& normal, const double radius )
	: GeometricObject( ),
	  p( p ),
	  normal( normal ),
	  r( radius ) {
	r_squared = radius * radius;
}


Disk::Disk( const Disk& disk ) = default;


Disk* Disk::clone( ) const {
	return new Disk( *this );
}


Disk::~Disk( ) = default;


Disk& Disk::operator=( const Disk& rhs ) {
	if( this == &rhs )
		return *this;

	GeometricObject::operator=( rhs );

	p = rhs.p;
	normal = rhs.normal;
	r = rhs.r;
	r_squared = rhs.r_squared;

	return *this;
}


bool Disk::hit( const Ray& ray, double& tmin, ShadeRec& sr ) const {

	const float t = ( p - ray.o ) * normal / ( ray.d * normal );

	if( t < kEpsilon )
		return false;

	const Point3D p = ray.o + t * ray.d;

	if( Disk::p.d_squared( p ) < r_squared ) {
		tmin = t;
		sr.normal = normal;
		if( Vector3D( ray.o ) * normal < 0.0 )
			sr.normal = -normal;

		return true;
	}

	return false;
}


bool Disk::shadow_hit( const Ray& ray, double& tmin ) const {
	const float t = ( p - ray.o ) * normal / ( ray.d * normal );

	if( t < kEpsilon )
		return false;

	const Point3D p = ray.o + t * ray.d;

	if( Disk::p.d_squared( p ) < r_squared )
		return true;

	return false;
}
