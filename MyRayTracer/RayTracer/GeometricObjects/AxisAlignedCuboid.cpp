#include "AxisAlignedCuboid.h"

#include "Point3D.h"
#include "Ray.h"
#include "ShadeRec.h"
#include "Constants.h"


AxisAlignedCuboid::AxisAlignedCuboid( )
	: GeometricObject( ),
	  x0( -1.0f ),
	  y0( -1.0f ),
	  z0( -1.0f ),
	  x1( 1.0f ),
	  y1( 1.0f ),
	  z1( 1.0f ) { }


AxisAlignedCuboid::AxisAlignedCuboid( double x0, double y0, double z0, double x1, double y1, double z1 )
	: GeometricObject( ),
	  x0( x0 ),
	  y0( y0 ),
	  z0( z0 ),
	  x1( x1 ),
	  y1( y1 ),
	  z1( z1 ) { }

AxisAlignedCuboid::AxisAlignedCuboid( Point3D p0, Point3D p1 )
	: GeometricObject( ),
	  x0( p0.x ),
	  y0( p0.y ),
	  z0( p0.z ),
	  x1( p1.x ),
	  y1( p1.y ),
	  z1( p1.z ) { }


AxisAlignedCuboid::AxisAlignedCuboid( const AxisAlignedCuboid& aacuboid ) = default;


AxisAlignedCuboid* AxisAlignedCuboid::clone( ) const {
	return new AxisAlignedCuboid( *this );
}


AxisAlignedCuboid::~AxisAlignedCuboid( ) = default;


AxisAlignedCuboid& AxisAlignedCuboid::operator=( const AxisAlignedCuboid& rhs ) {

	if( this == &rhs )
		return *this;

	GeometricObject::operator=( rhs );

	x0 = rhs.x0;
	y0 = rhs.y0;
	z0 = rhs.z1;
	x1 = rhs.x1;
	y1 = rhs.y1;
	z1 = rhs.z1;

	return *this;
}


bool AxisAlignedCuboid::hit( const Ray& ray, double& tmin, ShadeRec& sr ) const {

	const double ox = ray.o.x;
	const double oy = ray.o.y;
	const double oz = ray.o.z;
	const double dx = ray.d.x;
	const double dy = ray.d.y;
	const double dz = ray.d.z;

	double tx_min, ty_min, tz_min;
	double tx_max, ty_max, tz_max;

	const double a = 1.0f / dx;
	if( a >= 0.0f ) {
		tx_min = ( x0 - ox ) * a;
		tx_max = ( x1 - ox ) * a;
	}
	else {
		tx_min = ( x1 - ox ) * a;
		tx_max = ( x0 - ox ) * a;
	}

	const double b = 1.0f / dy;
	if( b >= 0.0f ) {
		ty_min = ( y0 - oy ) * b;
		ty_max = ( y1 - oy ) * b;
	}
	else {
		ty_min = ( y1 - oy ) * b;
		ty_max = ( y0 - oy ) * b;
	}

	const double c = 1.0f / dz;
	if( c >= 0.0f ) {
		tz_min = ( z0 - oz ) * c;
		tz_max = ( z1 - oz ) * c;
	}
	else {
		tz_min = ( z1 - oz ) * c;
		tz_max = ( z0 - oz ) * c;
	}

	double t0, t1;

	int face_entry, face_exit;

	if( tx_min > ty_min ) {
		t0 = tx_min;
		face_entry = a >= 0.0f ? 0 : 3;
	}
	else {
		t0 = ty_min;
		face_entry = b >= 0.0f ? 1 : 4;
	}
	if( tz_min > t0 ) {
		t0 = tz_min;
		face_entry = c >= 0.0 ? 2 : 5;
	}

	if( tx_max < ty_max ) {
		t1 = tx_max;
		face_exit = a >= 0.0f ? 3 : 0;
	}
	else {
		t1 = ty_max;
		face_exit = b >= 0.0f ? 4 : 1;
	}
	if( tz_max < t1 ) {
		t1 = tz_max;
		face_exit = c >= 0.0f ? 5 : 2;
	}

	if( t0 < t1 && t1 > kEpsilon ) {
		if( t0 > kEpsilon ) {
			tmin = t0;
			sr.normal = get_normal( face_entry );
		}
		else {
			tmin = t1;
			sr.normal = get_normal( face_exit );
		}

		sr.local_hit_point = ray.o + tmin * ray.d;

		return true;
	}

	return false;
}

bool AxisAlignedCuboid::shadow_hit( const Ray& ray, double& tmin ) const {
	const double ox = ray.o.x;
	const double oy = ray.o.y;
	const double oz = ray.o.z;
	const double dx = ray.d.x;
	const double dy = ray.d.y;
	const double dz = ray.d.z;

	double tx_min, ty_min, tz_min;
	double tx_max, ty_max, tz_max;

	const double a = 1.0f / dx;
	if( a >= 0.0f ) {
		tx_min = ( x0 - ox ) * a;
		tx_max = ( x1 - ox ) * a;
	}
	else {
		tx_min = ( x1 - ox ) * a;
		tx_max = ( x0 - ox ) * a;
	}

	const double b = 1.0f / dy;
	if( b >= 0.0f ) {
		ty_min = ( y0 - oy ) * b;
		ty_max = ( y1 - oy ) * b;
	}
	else {
		ty_min = ( y1 - oy ) * b;
		ty_max = ( y0 - oy ) * b;
	}

	const double c = 1.0f / dz;
	if( c >= 0.0f ) {
		tz_min = ( z0 - oz ) * c;
		tz_max = ( z1 - oz ) * c;
	}
	else {
		tz_min = ( z1 - oz ) * c;
		tz_max = ( z0 - oz ) * c;
	}

	double t0, t1;

	int face_entry, face_exit;

	if( tx_min > ty_min ) {
		t0 = tx_min;
		face_entry = a >= 0.0f ? 0 : 3;
	}
	else {
		t0 = ty_min;
		face_entry = b >= 0.0f ? 1 : 4;
	}
	if( tz_min > t0 ) {
		t0 = tz_min;
		face_entry = c >= 0.0 ? 2 : 5;
	}

	if( tx_max < ty_max ) {
		t1 = tx_max;
		face_exit = a >= 0.0f ? 3 : 0;
	}
	else {
		t1 = ty_max;
		face_exit = b >= 0.0f ? 4 : 1;
	}
	if( tz_max < t1 ) {
		t1 = tz_max;
		face_exit = c >= 0.0f ? 5 : 2;
	}

	if( t0 < t1 && t1 > kEpsilon ) {
		return true;
	}

	return false;
}


Normal AxisAlignedCuboid::get_normal( const int face_hit ) {
	switch( face_hit ) {
	case 0 :
		return Normal( -1.0, 0.0, 0.0 );
	case 1 :
		return Normal( 0.0, -1.0, 0.0 );
	case 2 :
		return Normal( 0.0, 0.0, -1.0 );
	case 3 :
		return Normal( 1.0, 0.0, 0.0 );
	case 4 :
		return Normal( 0.0, 1.0, 0.0 );
	case 5 :
		return Normal( 0.0, 0.0, 1.0 );
	default :
		return Normal( 0.0, 0.0, 0.0 );
	}
}
