#pragma once

#include "GeometricObject.h"
#include "Normal.h"
#include "Point3D.h"


class Triangle final : public GeometricObject {

public:

	Triangle( );
	Triangle( const Point3D& a, const Point3D& b, const Point3D& c );
	Triangle( const Triangle& triangle );
	Triangle* clone( ) const override;
	virtual ~Triangle( );

	Triangle& operator=( const Triangle& rhs );

	bool hit( const Ray& ray, double& tmin, ShadeRec& sr ) const override;

	bool shadow_hit( const Ray& ray, double& tmin ) const override;

private:

	Point3D a, b, c;
	Normal normal;

	void compute_normal( );

};
