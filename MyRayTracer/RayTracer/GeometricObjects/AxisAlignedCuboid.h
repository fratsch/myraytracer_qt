#pragma once

#include "GeometricObject.h"

class Point3D;
class Normal;


class AxisAlignedCuboid final : public GeometricObject {

public:

	AxisAlignedCuboid( );
	AxisAlignedCuboid( double x0, double y0, double z0, double x1, double y1, double z1 );
	AxisAlignedCuboid( Point3D p0, Point3D p1 );
	AxisAlignedCuboid( const AxisAlignedCuboid& aacuboid );
	AxisAlignedCuboid* clone( ) const override;
	virtual ~AxisAlignedCuboid( );

	AxisAlignedCuboid& operator=( const AxisAlignedCuboid& rhs );

	bool hit( const Ray& ray, double& tmin, ShadeRec& sr ) const override;

	bool shadow_hit( const Ray& ray, double& tmin ) const override;

private:

	double x0{ }, y0{ }, z0{ }, x1{ }, y1{ }, z1{ };

	static Normal get_normal( int face_hit );

};
