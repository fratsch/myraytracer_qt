#pragma once

#include "GeometricObject.h"
#include "Normal.h"
#include "Vector3D.h"
#include "Point3D.h"

class Normal;
class Vector3D;
class Point3D;


class Rectangle : public GeometricObject {

public:

	Rectangle( );
	Rectangle( const Point3D& p, const Vector3D& a, const Vector3D b );
	Rectangle( const Point3D& p, const Vector3D& a, const Vector3D b, Normal& n );
	Rectangle( const Rectangle& rect );
	Rectangle* clone( ) const override;
	virtual ~Rectangle( );

	Rectangle& operator=( const Rectangle& rhs );

	bool hit( const Ray& ray, double& tmin, ShadeRec& sr ) const override;

	bool shadow_hit( const Ray& ray, double& tmin ) const override;

private:
	// @formatter:off
	
	Point3D		p;
	Vector3D	a;
	Vector3D	b;
	double		a_len_squared;
	double		b_len_squared;
	Normal		normal;

	// @formatter:on
};
