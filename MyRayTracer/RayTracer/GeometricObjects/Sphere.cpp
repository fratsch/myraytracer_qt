#include "Sphere.h"

#include <cmath>

#include "Ray.h"
#include "ShadeRec.h"
#include "Constants.h"


Sphere::Sphere( )
	: BoundedObject( ),
	  center( 0.0 ),
	  radius( 1.0 ) {
	create_bounding_box( );
}


Sphere::Sphere( Point3D c, double r )
	: BoundedObject( ),
	  center( c ),
	  radius( r ) {
	create_bounding_box( );
}


Sphere* Sphere::clone( ) const {
	return new Sphere( *this );
}


Sphere::Sphere( const Sphere& sphere )
	: BoundedObject( sphere ),
	  center( sphere.center ),
	  radius( sphere.radius ) {
	bbox = sphere.bbox;
}


Sphere& Sphere::operator=( const Sphere& rhs ) {
	if( this == &rhs )
		return *this;

	GeometricObject::operator=( rhs );

	center = rhs.center;
	radius = rhs.radius;
	bbox = rhs.bbox;

	return *this;
}


Sphere::~Sphere( ) = default;


bool Sphere::hit( const Ray& ray, double& tmin, ShadeRec& sr ) const {
	const Vector3D temp = ray.o - center;
	const double a = ray.d * ray.d;
	const double b = 2.0 * temp * ray.d;
	const double c = temp * temp - radius * radius;
	const double disc = b * b - 4.0 * a * c;

	if( disc < 0.0 )
		return false;

	const double e = sqrt( disc );
	const double denom = 2.0 * a;
	double t = ( -b - e ) / denom;    // smaller root

	if( t > kEpsilon ) {
		tmin = t;
		sr.normal = ( temp + t * ray.d ) / radius;
		sr.local_hit_point = ray.o + t * ray.d;
		return  true ;
	}

	t = ( -b + e ) / denom;    // larger root

	if( t > kEpsilon ) {
		tmin = t;
		sr.normal = ( temp + t * ray.d ) / radius;
		sr.local_hit_point = ray.o + t * ray.d;

		return true;
	}

	return false;
}

bool Sphere::shadow_hit( const Ray& ray, double& tmin ) const {

	if( !shadows )
		return false;

	const Vector3D temp = ray.o - center;
	const double a = ray.d * ray.d;
	const double b = 2.0 * temp * ray.d;
	const double c = temp * temp - radius * radius;
	const double disc = b * b - 4.0 * a * c;

	if( disc < 0.0 )
		return false;

	const double e = sqrt( disc );
	const double denom = 2.0 * a;
	double t = ( -b - e ) / denom;    // smaller root

	if( t > kEpsilon ) {
		tmin = t;
		return true;
	}

	t = ( -b + e ) / denom;    // larger root

	if( t > kEpsilon ) {
		tmin = t;

		return true;
	}

	return false;
}

void Sphere::create_bounding_box( ) {
	const Point3D p0( center.x - radius - kEpsilon, center.y - radius - kEpsilon, center.z - radius - kEpsilon );
	const Point3D p1( center.x + radius + kEpsilon, center.y + radius + kEpsilon, center.z + radius + kEpsilon );

	bbox = BoundingBox( p0, p1 );
}
