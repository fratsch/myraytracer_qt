#pragma once

#include "GeometricObject.h"

#include <vector>


class Compound : public GeometricObject {

public:

	Compound( );
	Compound( const Compound& comp );
	Compound* clone( ) const override;
	~Compound( );

	Compound& operator=( const Compound& rhs );

	void set_material( Material* material_ptr ) override;

	void add_object( GeometricObject* object_ptr );

	int get_num_objects( ) const;

	bool hit( const Ray& ray, double& tmin, ShadeRec& sr ) const override;

protected:

	std::vector<GeometricObject*> objects;

private:

	void delete_objects( );

	void copy_objects( const std::vector<GeometricObject*>& rhs_objects );

};


inline int Compound::get_num_objects( ) const {
	return objects.size( );
}
