#pragma once

#include "GeometricObject.h"

#include "BoundingBox.h"


class BoundedObject : public GeometricObject {

public:

	BoundedObject( );
	explicit BoundedObject( const BoundingBox& bounding_box, bool shadows = true );
	BoundedObject( const BoundedObject& bounded_object );
	BoundedObject* clone( ) const override = 0;
	~BoundedObject( ) override;

	BoundedObject& operator=( const BoundedObject& rhs );

	bool hit(const Ray& ray, double& tmin, ShadeRec& sr) const override = 0;

	bool shadow_hit(const Ray& ray, double& tmin) const override = 0;

	virtual void set_bounding_box( const BoundingBox& bounding_box );

	virtual BoundingBox get_bounding_box( ) const;

protected:

	BoundingBox bbox;

};
