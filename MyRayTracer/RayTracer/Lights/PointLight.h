#pragma once

#include "Light.h"
#include "Point3D.h"


class PointLight final : public Light {

public:

	PointLight( );
	PointLight( const PointLight& point_light );
	PointLight( float scale_radiance, const RGBColor& color, const Point3D& location );
	virtual ~PointLight( );

	PointLight& operator=( const PointLight& rhs );

	Vector3D get_direction( ShadeRec& sr ) override;

	RGBColor L( ShadeRec& sr ) override;

	bool in_shadow( const Ray& ray, const ShadeRec& sr ) const override;

	void scale_radiance(float scale);

	void set_color(float c);

	void set_color(const RGBColor& c);

	void set_color(float r, float g, float b);

	void set_location( const Point3D& loc);

	void set_location(double x, double y, double z);

private:

	float ls{ };
	RGBColor color;
	Point3D location;

};
