#pragma once

#include "Light.h"


class Directional final : public Light {

public:

	Directional( );
	Directional( const Directional& directional );
	Directional( float scale_radiance, const RGBColor& color, const Vector3D& direction );
	virtual ~Directional( );

	Directional& operator=( const Directional& rhs );

	Vector3D get_direction( ShadeRec& s ) override;

	RGBColor L( ShadeRec& sr ) override;

	bool in_shadow( const Ray& ray, const ShadeRec& sr ) const override;

	void scale_radiance(float scale);

	void set_color(float c);

	void set_color(const RGBColor& c);

	void set_color(float r, float g, float b);

private:

	float ls{ };
	RGBColor color;
	Vector3D direction;

};
