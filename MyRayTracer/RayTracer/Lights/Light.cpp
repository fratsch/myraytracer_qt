#include "Light.h"


Light::Light( )
	: shadows( true ) {}


Light::Light( const bool shadows )
	: shadows( shadows ) {}


Light::Light( const Light& ls ) = default;


Light::~Light( ) = default;


Light& Light::operator=( const Light& rhs ) {
	if( this == &rhs )
		return *this;

	shadows = rhs.shadows;

	return *this;
}
