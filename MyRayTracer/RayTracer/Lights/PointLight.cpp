#include "PointLight.h"

#include "ShadeRec.h"
#include "World.h"
#include "GeometricObject.h"


PointLight::PointLight( )
	: Light( true ),
	  ls( 1.0f ),
	  color( 1.0f ),
	  location( Point3D( ) ) { }


PointLight::PointLight( const PointLight& point_light ) = default;


PointLight::PointLight( const float scale_radiance, const RGBColor& color, const Point3D& location )
	: Light( ),
	  ls( scale_radiance ),
	  color( color ),
	  location( location ) { }


PointLight& PointLight::operator=( const PointLight& rhs ) {
	if( this == &rhs )
		return *this;

	Light::operator=( rhs );

	ls = rhs.ls;
	color = rhs.color;
	location = rhs.location;

	return *this;
}


PointLight::~PointLight( ) = default;


Vector3D PointLight::get_direction( ShadeRec& s ) {
	return ( location - s.hit_point ).hat( );
}


RGBColor PointLight::L( ShadeRec& s ) {
	return ls * color;
}

bool PointLight::in_shadow( const Ray& ray, const ShadeRec& sr ) const {
	double t;
	const int num_objects = sr.w.objects.size( );
	const double d = location.distance( ray.o );

	for( int j = 0 ; j < num_objects ; j++ )
		if( sr.w.objects[j]->shadow_hit( ray, t ) && t < d )
			return true;

	return false;
}


void PointLight::scale_radiance( float scale ) {
	ls = scale;
}


void PointLight::set_color( float c ) {
	color = RGBColor( c );
}


void PointLight::set_color( const RGBColor& c ) {
	color = c;
}


void PointLight::set_color( float r, float g, float b ) {
	color = RGBColor( r, g, b );
}


void PointLight::set_location( const Point3D& loc ) {
	location = loc;
}


void PointLight::set_location( double x, double y, double z ) {
	location = Point3D( x, y, z );
}
