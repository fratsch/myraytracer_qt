#pragma once

#include "Vector3D.h"
#include "RGBColor.h"

class Ray;
class ShadeRec;


class Light {

public:

	Light( );
	explicit Light( bool shadows );
	Light( const Light& ls );
	virtual ~Light( );

	Light& operator=( const Light& rhs );

	virtual Vector3D get_direction( ShadeRec& sr ) = 0;

	virtual RGBColor L( ShadeRec& sr ) = 0;

	virtual bool in_shadow( const Ray& ray, const ShadeRec& sr ) const = 0;

	bool casts_shadows( ) const;

private:

	bool shadows{ };

};


inline bool Light::casts_shadows( ) const {
	return shadows;
}
