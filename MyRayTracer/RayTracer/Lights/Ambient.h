#pragma once

#include "Light.h"


class Ambient final : public Light {

public:

	Ambient( );
	Ambient( const Ambient& ambient );
	Ambient( float scale_radiance, const RGBColor& color );
	virtual ~Ambient( );

	Ambient& operator=( const Ambient& rhs );

	void scale_radiance( float b );

	void set_color( float c );

	void set_color( const RGBColor& c );

	void set_color( float r, float g, float b );

	Vector3D get_direction( ShadeRec& sr ) override;

	RGBColor L( ShadeRec& sr ) override;

	bool in_shadow( const Ray& ray, const ShadeRec& sr ) const override;

private:

	float ls{ };
	RGBColor color;

};


inline void Ambient::scale_radiance( const float b ) {
	ls = b;
}


inline void Ambient::set_color( const float c ) {
	color.r = c;
	color.g = c;
	color.b = c;
}


inline void Ambient::set_color( const RGBColor& c ) {
	color = c;
}


inline void Ambient::set_color( const float r, const float g, const float b ) {
	color.r = r;
	color.g = g;
	color.b = b;
}
