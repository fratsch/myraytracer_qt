#include "Directional.h"

#include "ShadeRec.h"
#include "World.h"
#include "GeometricObject.h"
#include "Constants.h"


Directional::Directional( )
	: Light( true ),
	  ls( 1.0 ),
	  color( 1.0 ),
	  direction( Vector3D( ) ) {}


Directional::Directional( const Directional& directional ) = default;


Directional::Directional( const float scale_radiance, const RGBColor& color, const Vector3D& direction )
	: Light( ),
	  ls( scale_radiance ),
	  color( color ),
	  direction( direction ) {}


Directional& Directional::operator=( const Directional& rhs ) {
	if( this == &rhs )
		return *this;

	Light::operator=( rhs );

	ls = rhs.ls;
	color = rhs.color;
	direction = rhs.direction;

	return *this;
}


Directional::~Directional( ) = default;


Vector3D Directional::get_direction( ShadeRec& s ) {
	return -direction;
}


RGBColor Directional::L( ShadeRec& sr ) {
	return ls * color;
}

bool Directional::in_shadow( const Ray& ray, const ShadeRec& sr ) const {
	double t;
	const int num_objects = sr.w.objects.size( );

	for( int j = 0 ; j < num_objects ; ++j ) {
		if( sr.w.objects[j]->shadow_hit( ray, t ) && t < kHugeValue )
			return true;
	}

	return false;
}


void Directional::scale_radiance( const float scale ) {
	ls = scale;
}


void Directional::set_color( const float c ) {
	color = RGBColor( c );
}


void Directional::set_color( const RGBColor& c ) {
	color = c;
}


void Directional::set_color( const float r, const float g, const float b ) {
	color = RGBColor( r, g, b );
}
