#pragma once

#include <QThread>
#include "RayTracerGUI.h"


class WorldWorker : public QThread {

Q_OBJECT

public:

	WorldWorker( WorldConfig* world_config, SamplerConfig* sampler_config, AmbientConfig* ambient_config, CameraConfig* camera_config, QObject* parent = Q_NULLPTR );

	~WorldWorker( );

	void run( ) override;

signals:
	
	void resultReady( QImage result );

private:

	SamplerConfig* sampler_config;

	CameraConfig* camera_config;

	AmbientConfig* ambient_config;
	
	WorldConfig* world_config;

};
