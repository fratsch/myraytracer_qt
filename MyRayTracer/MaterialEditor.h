#pragma once


#include "ui_MaterialEditor.h"


class WorldConfig;


class MaterialEditor : public QDialog {

Q_OBJECT

public:

	MaterialEditor( QWidget* parent = Q_NULLPTR );
	~MaterialEditor( );

public slots:

	// close button action
	void on_pushButton_Close_MaterialEditor_clicked( );

	// add/edit button action
	void on_pushButton_AddEdit_Material_clicked( );

private:

	Ui::MaterialEditor ui;

};
