#include "WorldConfig.h"

#include <QDebug>

#include "ObjectEditorItem.h"
#include "MaterialEditorItem.h"
#include "LightEditorItem.h"
#include "Directional.h"
#include "PointLight.h"
#include "Matte.h"
#include "Sphere.h"
#include "Plane.h"
#include "AxisAlignedCuboid.h"
#include "Triangle.h"
#include "Rectangle.h"
#include "Disk.h"
#include "Phong.h"
#include "Regular.h"


WorldConfig::WorldConfig( ) {
	//add_light( new LightEditorItem( new PointLight( 1.0f, RGBColor( 1.0f, 1.0f, 1.0f ), Point3D( -1000.0, 1000.0f, 300.0f ) ), "Point Light" ) );

	add_light( new LightEditorItem( new Directional( 1.0f, RGBColor( 1.0f, 1.0f, 1.0f ), Vector3D( 1.0f, -1.0f, -1.0f ) ), "Directional Light" ) );

	add_material( new MaterialEditorItem( new Matte( true, new Lambertian( 0.25f, RGBColor( 1.0f, 0.0f, 0.0f ) ),
	                                                 new Lambertian( 0.65f, RGBColor( 1.0f, 0.0f, 0.0f ) ) ), "Default Matte Red" ) );
	add_material( new MaterialEditorItem( new Matte( true, new Lambertian( 0.25f, RGBColor( 0.0f, 1.0f, 0.0f ) ),
	                                                 new Lambertian( 0.65f, RGBColor( 0.0f, 1.0f, 0.0f ) ) ), "Default Matte Green" ) );
	add_material( new MaterialEditorItem( new Matte( true, new Lambertian( 0.25f, RGBColor( 0.0f, 0.0f, 1.0f ) ),
	                                                 new Lambertian( 0.65f, RGBColor( 0.0f, 0.0f, 1.0f ) ) ), "Default Matte Blue" ) );
	add_material( new MaterialEditorItem( new Phong( true, new Lambertian( 0.25f, RGBColor( 1.0f, 0.0f, 0.0f ) ),
	                                                 new Lambertian( 0.6f, RGBColor( 1.0f, 0.0f, 0.0f ) ),
	                                                 new GlossySpecular( 0.5f, RGBColor( 1.0f, 0.0f, 0.0f ), 32,
	                                                                     new Regular( 16, 32 ) ) ), "Default Phong Red" ) );

	GeometricObject* object;

	/*object = new Sphere( Point3D( 0.0, 0.0, 0.0 ), 40.0 );
	object->set_material( materials[0]->get_material( ) );
	add_object( new ObjectEditorItem( object, "Sphere" ) );*/

	int object_count = 1000;


	for( int i = 0 ; i < object_count ; ++i ) {
		Point3D p( rand( ) % 1000 + 1.0, rand( ) % 1000 - 499.0, rand( ) % 1000 - 499.0 );
		double radius = rand( ) % 10 + 1.0;
		object = new Sphere( p, radius );
		object->set_material( materials[rand( ) % 3]->get_material( ) );
		add_object( new ObjectEditorItem( object, "Random Sphere " + QString::number( i ) ) );
	}


	/*object = new Sphere( Point3D( 0.0, 0.0, 0.0 ), 60.0 );

	object->set_material( materials[3]->get_material( ) );

	add_object( new ObjectEditorItem( object, "Default Sphere Red" ) );

	object = new Sphere( Point3D( -55.0, 55.0, 55.0 ), 25.0 );

	object->set_material( materials[3]->get_material( ) );

	add_object( new ObjectEditorItem( object, "Default Sphere Green" ) );

	object = new AxisAlignedCuboid( -30.0, -60.0, -130.0, 30.0, 80.0, -70.0 );

	object->set_material( materials[0]->get_material( ) );

	add_object( new ObjectEditorItem( object, "Cuboid Red" ) );

	object = new Triangle( Point3D( 0.0, 80.0, -60.0 ), Point3D( 0.0, 120.0, 10.0 ), Point3D( 0.0, 120.0, -50.0 ) );

	object->set_material( materials[0]->get_material( ) );

	add_object( new ObjectEditorItem( object, "Triangle Red" ) );

	object = new Disk( Point3D( -40.0, -40.0, 80.0 ), Normal( 0.0, 1.0, 0.0 ), 20.0 );

	object->set_material( materials[0]->get_material( ) );

	add_object( new ObjectEditorItem( object, "Rectangle Red" ) );

	object = new Rectangle( Point3D( 0.0, -40.0, 80.0 ), Vector3D( 0.0, 60.0, 0.0 ), Vector3D( 0.0, 0.0, 40.0 ) );

	object->set_material( materials[0]->get_material( ) );

	add_object( new ObjectEditorItem( object, "Rectangle Red" ) );

	object = new Plane( Point3D( 80.0, 0.0, 0.0 ), Normal( -1.0, 0.0, 0.0 ) );

	object->set_material( materials[2]->get_material( ) );

	add_object( new ObjectEditorItem( object, "Background Plane" ) );

	object = new Plane( Point3D( 0.0, -80.0, 0.0 ), Normal( 0.0, -1.0, 0.0 ) );

	object->set_material( materials[1]->get_material( ) );

	add_object( new ObjectEditorItem( object, "Ground Plane" ) );*/

	object = nullptr;
}


WorldConfig::~WorldConfig( ) {
	delete_objects( );
	delete_materials( );
	delete_lights( );
}


void WorldConfig::add_object( ObjectEditorItem* object ) {
	objects.push_back( object );
}


void WorldConfig::delete_object( ObjectEditorItem* object ) {
	const int index = objects.indexOf( object );

	if( index >= 0 ) {
		delete objects[index];
		objects[index] = nullptr;
		objects.erase( objects.begin( ) + index );
	}
}


void WorldConfig::add_material( MaterialEditorItem* material ) {
	materials.push_back( material );
}


void WorldConfig::delete_material( MaterialEditorItem* material ) {
	const int index = materials.indexOf( material );

	if( index >= 0 ) {
		delete materials[index];
		materials[index] = nullptr;
		materials.erase( materials.begin( ) + index );
	}
}


void WorldConfig::add_light( LightEditorItem* light ) {
	lights.push_back( light );
}


void WorldConfig::delete_light( LightEditorItem* light ) {
	const int index = lights.indexOf( light );

	if( index >= 0 ) {
		delete lights[index];
		lights[index] = nullptr;
		lights.erase( lights.begin( ) + index );
	}
}


void WorldConfig::delete_objects( ) {
	const int num_objects = objects.size( );

	for( int i = 0 ; i < num_objects ; ++i ) {
		delete objects[i];
		objects[i] = nullptr;
	}

	objects.erase( objects.begin( ), objects.end( ) );
}


void WorldConfig::delete_materials( ) {
	const int num_objects = materials.size( );

	for( int i = 0 ; i < num_objects ; ++i ) {
		delete materials[i];
		materials[i] = nullptr;
	}

	materials.erase( materials.begin( ), materials.end( ) );
}


void WorldConfig::delete_lights( ) {
	const int num_objects = lights.size( );

	for( int i = 0 ; i < num_objects ; ++i ) {
		delete lights[i];
		lights[i] = nullptr;
	}

	lights.erase( lights.begin( ), lights.end( ) );
}
