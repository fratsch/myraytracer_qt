#pragma once


class AmbientConfig {

public:

	AmbientConfig( );
	~AmbientConfig( );

	void set_ambient_color( float r, float g, float b );

	// ambient color
	float amb_r, amb_g, amb_b;

	// radiance scaling factor
	float radiance_scaling_factor;

};


inline void AmbientConfig::set_ambient_color( float r, float g, float b ) {
	amb_r = r;
	amb_g = g;
	amb_b = b;
}
