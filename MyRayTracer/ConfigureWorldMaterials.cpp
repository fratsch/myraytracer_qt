#include "ConfigureWorldMaterials.h"


#include "MaterialEditor.h"
#include "MaterialEditorItem.h"
#include "WorldConfig.h"


ConfigureWorldMaterials::ConfigureWorldMaterials( WorldConfig* world_config, QWidget* parent )
	: QDialog( parent ),
	  world_config( world_config ) {

	ui.setupUi( this );

	for( MaterialEditorItem* item : world_config->get_materials( ) ) {
		ui.listWidget_worldMaterials->addItem( item );
	}
}

ConfigureWorldMaterials::~ConfigureWorldMaterials( ) = default;

void ConfigureWorldMaterials::add_WorldMaterial( Material* material, QString material_name ) {
	MaterialEditorItem* item = new MaterialEditorItem( material, material_name );
	ui.listWidget_worldMaterials->addItem( item );
	world_config->add_material( item );
}

void ConfigureWorldMaterials::on_pushButton_Close_ConfigureWorldMaterials_clicked( ) {
	for (int i = ui.listWidget_worldMaterials->count() - 1; i >= 0; --i) {
		ui.listWidget_worldMaterials->takeItem(i);
	}
	close( );
}

void ConfigureWorldMaterials::on_pushButton_AddMaterial_clicked( ) {
	MaterialEditor material_editor_window( this );
	material_editor_window.setModal( true );
	material_editor_window.exec( );
}

void ConfigureWorldMaterials::on_pushButton_DeleteMaterial_clicked( ) {
	if( ui.listWidget_worldMaterials->currentItem( ) != nullptr ) {
		QListWidgetItem* item = ui.listWidget_worldMaterials->takeItem( ui.listWidget_worldMaterials->currentRow( ) );
		world_config->delete_material( dynamic_cast<MaterialEditorItem*>( item ) );
		delete item;
		item = nullptr;
	}
}

void ConfigureWorldMaterials::on_pushButton_EditMaterial_clicked( ) {}
