#include "AmbientConfig.h"


AmbientConfig::AmbientConfig( )
	: amb_r( 1.0f ),
	  amb_g( 1.0f ),
	  amb_b( 1.0f ),
	  radiance_scaling_factor( 1.0f ) { }


AmbientConfig::~AmbientConfig( ) = default;
