#include "CameraConfig.h"


CameraConfig::CameraConfig( )
	: camera( Cameras::PIN ),
	  hres( 800 ),
	  vres( 600 ),
	  pixel_size( 1.0f ),
	  cam_x( -200.0f ),
	  cam_y( 0.0f ),
	  cam_z( 0.0f ),
	  lookAt_x( 0.0f ),
	  lookAt_y( 0.0f ),
	  lookAt_z( 0.0f ),
	  up_x( 0.0f ),
	  up_y( 1.0f ),
	  up_z( 0.0f ),
	  rolling_angle( 0.0f ),
	  view_distance( 400.0f ),
	  zoom( 1.0f ),
	  exposure( 1.0f ),
	  focal_distance( 500.0f ),
	  lens_radius( 1.0f ) {}


CameraConfig::~CameraConfig( ) = default;
