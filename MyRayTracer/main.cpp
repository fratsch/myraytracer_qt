#include "RayTracerGUI.h"
#include <QtWidgets/QApplication>


int main( int argc, char* argv[] ) {
	QApplication a( argc, argv );
	RayTracerGUI w;
	w.setWindowTitle( "MyRayTracer" );
	w.show( );

	return a.exec( );
}
