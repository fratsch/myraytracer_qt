#include "SamplerConfig.h"


SamplerConfig::SamplerConfig( )
	: sampler( Samplers::REG ),
	  num_samples( 1 ),
	  num_sets( 1 ) {}


SamplerConfig::~SamplerConfig( ) = default;
