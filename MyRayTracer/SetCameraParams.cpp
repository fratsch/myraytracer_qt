#include "SetCameraParams.h"


SetCameraParams::SetCameraParams( CameraConfig* camera_config, QWidget* parent )
	: QDialog( parent ),
	  camera_config( camera_config ) {

	ui.setupUi( this );

	ui.comboBox_cameraType->addItems( { "Orthographic", "Pinhole", "Thinlense" } );
	ui.comboBox_cameraType->setCurrentIndex( static_cast<int>( camera_config->camera ) );

	ui.spinBox_hres->setValue( camera_config->hres );
	ui.spinBox_vres->setValue( camera_config->vres );
	ui.doubleSpinBox_pixelSize->setValue( camera_config->pixel_size );

	ui.doubleSpinBox_xPos->setValue( camera_config->cam_x );
	ui.doubleSpinBox_yPos->setValue( camera_config->cam_y );
	ui.doubleSpinBox_zPos->setValue( camera_config->cam_z );

	ui.doubleSpinBox_xLookAt->setValue( camera_config->lookAt_x );
	ui.doubleSpinBox_yLookAt->setValue( camera_config->lookAt_y );
	ui.doubleSpinBox_zLookAt->setValue( camera_config->lookAt_z );

	ui.doubleSpinBox_xPos_up->setValue( camera_config->up_x );
	ui.doubleSpinBox_yPos_up->setValue( camera_config->up_y );
	ui.doubleSpinBox_zPos_up->setValue( camera_config->up_z );

	ui.doubleSpinBox_rollingAngle->setValue( camera_config->rolling_angle );
	ui.doubleSpinBox_viewDistance->setValue( camera_config->view_distance );
	ui.doubleSpinBox_zoom->setValue( camera_config->zoom );
	ui.doubleSpinBox_exposure->setValue( camera_config->exposure );
}


SetCameraParams::~SetCameraParams( ) = default;


void SetCameraParams::on_pushButton_Cancel_Camera_clicked( ) {
	close( );
}


void SetCameraParams::on_pushButton_Confirm_Camera_clicked( ) {
	camera_config->camera = static_cast<Cameras>( ui.comboBox_cameraType->currentIndex( ) );

	camera_config->set_resolution( ui.spinBox_hres->value( ), ui.spinBox_vres->value( ) );
	camera_config->pixel_size = static_cast<float>( ui.doubleSpinBox_pixelSize->value( ) );

	camera_config->set_camera_position( static_cast<float>( ui.doubleSpinBox_xPos->value( ) ), static_cast<float>( ui.doubleSpinBox_yPos->value( ) ),
	                                    static_cast<float>( ui.doubleSpinBox_zPos->value( ) ) );

	camera_config->set_lookAt_position( static_cast<float>( ui.doubleSpinBox_xLookAt->value( ) ), static_cast<float>( ui.doubleSpinBox_yLookAt->value( ) ),
	                                    static_cast<float>( ui.doubleSpinBox_zLookAt->value( ) ) );

	camera_config->set_up_vector( static_cast<float>( ui.doubleSpinBox_xPos_up->value( ) ), static_cast<float>( ui.doubleSpinBox_yPos_up->value( ) ),
	                              static_cast<float>( ui.doubleSpinBox_zPos_up->value( ) ) );

	camera_config->rolling_angle = static_cast<float>( ui.doubleSpinBox_rollingAngle->value( ) );
	camera_config->view_distance = static_cast<float>( ui.doubleSpinBox_viewDistance->value( ) );
	camera_config->zoom = static_cast<float>( ui.doubleSpinBox_zoom->value( ) );
	camera_config->exposure = static_cast<float>( ui.doubleSpinBox_exposure->value( ) );

	close( );
}
