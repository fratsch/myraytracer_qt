#include "LightEditor.h"

#include "ConfigureWorldLights.h"
#include "Directional.h"
#include "PointLight.h"


LightEditor::LightEditor( QWidget* parent )
	: QDialog( parent ) {
	ui.setupUi( this );
}

LightEditor::~LightEditor( ) {}

void LightEditor::on_pushButton_Close_LightEditor_clicked( ) {
	close( );
}

void LightEditor::on_pushButton_AddEdit_Light_clicked( ) {
	Light* light;
	RGBColor color;
	Point3D vec;

	switch( ui.tabWidget_LightParameters->currentIndex( ) ) {
	case 0 :
		color = RGBColor( ui.doubleSpinBox_Directional_colorR->value( ), ui.doubleSpinBox_Directional_colorG->value( ),
		                  ui.doubleSpinBox_Directional_colorB->value( ) );
		vec = Point3D( ui.doubleSpinBox_Directional_xDir->value( ), ui.doubleSpinBox_Directional_yDir->value( ), ui.doubleSpinBox_Directional_zDir->value( ) );
		light = new Directional( ui.doubleSpinBox_Directional_radianceScalingFactor->value( ), color, Vector3D( vec ) );
		break;
	case 1 :
		color = RGBColor( ui.doubleSpinBox_Directional_colorR->value( ), ui.doubleSpinBox_Directional_colorG->value( ),
		                  ui.doubleSpinBox_Directional_colorB->value( ) );
		vec = Point3D( ui.doubleSpinBox_Directional_xDir->value( ), ui.doubleSpinBox_Directional_yDir->value( ), ui.doubleSpinBox_Directional_zDir->value( ) );
		light = new PointLight( ui.doubleSpinBox_Directional_radianceScalingFactor->value( ), color, vec );
		break;
	default :
		light = new Directional( 1.0f, RGBColor( 1.0f, 1.0f, 1.0f ), Vector3D( 0.0f, -1.0f, 0.0f ) );
		break;
	}
	dynamic_cast<ConfigureWorldLights*>( parentWidget( ) )->add_WorldLight( light, ui.lineEdit_lightName->text( ) );
	light = nullptr;

	close( );
}
