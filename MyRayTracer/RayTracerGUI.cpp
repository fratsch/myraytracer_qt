#include "RayTracerGUI.h"

#include <QDebug>
#include <QGraphicsScene>

#include "World.h"
#include "WorldWorker.h"
#include "SetSamplerParams.h"
#include "SetCameraParams.h"
#include "SetAmbientParams.h"
#include "CameraConfig.h"
#include "AmbientConfig.h"
#include "ConfigureWorldObjects.h"
#include "ConfigureWorldLights.h"
#include "ConfigureWorldMaterials.h"


RayTracerGUI::RayTracerGUI( QWidget* parent )
	: QMainWindow( parent ),
	  sampler_config( new SamplerConfig( ) ),
	  camera_config( new CameraConfig( ) ),
	  world_config( new WorldConfig ),
	  ambient_config( new AmbientConfig ) {

	ui.setupUi( this );
}

RayTracerGUI::~RayTracerGUI( ) = default;


void RayTracerGUI::on_actionExit_triggered( ) {
	QApplication::exit( );
}


void RayTracerGUI::on_actionStart_triggered( ) {

	WorldWorker* world_Worker = new WorldWorker( world_config, sampler_config, ambient_config, camera_config, this );

	connect( world_Worker, &WorldWorker::resultReady, this, &RayTracerGUI::drawRenderedImage );
	connect( world_Worker, &WorldWorker::finished, world_Worker, &QObject::deleteLater );

	world_Worker->start( );
}


void RayTracerGUI::on_actionClear_triggered( ) {
	QImage clear_image = QImage( 1024, 768, QImage::Format_RGB32 );
	clear_image.fill( Qt::white );
	QGraphicsScene* scene = new QGraphicsScene( this );

	scene->addPixmap( QPixmap::fromImage( clear_image ) );

	ui.graphicsView->setScene( scene );
}


void RayTracerGUI::on_actionConfigure_Sampling_triggered( ) {
	SetSamplerParams sampler_params_window( sampler_config, this );
	sampler_params_window.setModal( true );
	sampler_params_window.exec( );
}


void RayTracerGUI::on_actionConfigure_Camera_triggered( ) {
	SetCameraParams camera_params_window( camera_config, this );
	camera_params_window.setModal( true );
	camera_params_window.exec( );
}

void RayTracerGUI::on_actionConfigure_Ambient_triggered( ) {
	SetAmbientParams ambient_params_window( ambient_config, this );
	ambient_params_window.setModal( true );
	ambient_params_window.exec( );
}


void RayTracerGUI::on_actionConfigure_Objects_triggered( ) {
	ConfigureWorldObjects object_editor_window( world_config, this );
	object_editor_window.setModal( true );
	object_editor_window.exec( );
}


void RayTracerGUI::on_actionConfigure_Materials_triggered( ) {
	ConfigureWorldMaterials material_editor_window( world_config, this );
	material_editor_window.setModal( true );
	material_editor_window.exec( );
}


void RayTracerGUI::on_actionConfigure_Lights_triggered( ) {
	ConfigureWorldLights light_editor_window( world_config, this );
	light_editor_window.setModal( true );
	light_editor_window.exec( );
}


void RayTracerGUI::drawRenderedImage( QImage image ) {

	qInfo( ) << "Drawing Image!";

	QImage scaledImage = image.scaled( 1024, 768, Qt::KeepAspectRatio );

	QGraphicsScene* scene = new QGraphicsScene( this );

	scene->addPixmap( QPixmap::fromImage( scaledImage ) );

	ui.graphicsView->setScene( scene );

	qInfo( ) << "Image drawn!";
}
