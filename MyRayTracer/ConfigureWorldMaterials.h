#pragma once


#include "ui_ConfigureWorldMaterials.h"


class Material;
class WorldConfig;


class ConfigureWorldMaterials : public QDialog {

Q_OBJECT

public:

	ConfigureWorldMaterials( WorldConfig* world_config, QWidget* parent = Q_NULLPTR );
	~ConfigureWorldMaterials( );

	void add_WorldMaterial( Material* material, QString material_name );

public slots:

	// close button action
	void on_pushButton_Close_ConfigureWorldMaterials_clicked( );

	// add material button action
	void on_pushButton_AddMaterial_clicked( );

	// delete material button action
	void on_pushButton_DeleteMaterial_clicked( );

	// edit material button action
	void on_pushButton_EditMaterial_clicked( );

private:

	Ui::ConfigureWorldMaterials ui;

	WorldConfig* world_config;

};
