#include "OGLWidget.h"

OGLWidget::OGLWidget( QWidget* parent )
	: QOpenGLWidget( parent ) {}

OGLWidget::~OGLWidget( ) {}

void OGLWidget::initializeGL( ) { }

void OGLWidget::resizeGL( int w, int h ) {}

void OGLWidget::paintGL( ) {}

void OGLWidget::clear( ) {
	QPainter painter(this);
	painter.setPen(Qt::black);
	painter.drawRect(0,0, rect().width(  ), rect().height(  ));

	painter.end();
}

