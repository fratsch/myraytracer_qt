#pragma once


#include "ui_ConfigureWorldLights.h"


class Light;
class WorldConfig;


class ConfigureWorldLights : public QDialog {

Q_OBJECT

public:

	ConfigureWorldLights( WorldConfig* world_config, QWidget* parent = Q_NULLPTR );
	~ConfigureWorldLights( );

	void add_WorldLight( Light* light, QString light_name );

public slots:

	// close button action
	void on_pushButton_Close_ConfigureWorldLights_clicked( );

	// add light button action
	void on_pushButton_AddLight_clicked( );

	// delete light button action
	void on_pushButton_DeleteLight_clicked( );

	// edit light button action
	void on_pushButton_EditLight_clicked( );

private:

	Ui::ConfigureWorldLights ui;

	WorldConfig* world_config;

};
